var limites = new Array();
var campos = new Array();// los campos para la SQL
var mcampos = new Array();// todos los campos que mostraremos
var columnas = new Array();
var boton = new Array("finIz", "iz", "de", "finDe", "mas", "menos");
var cuantos; //cuantos registro hay en total
var campoId; //campo indice identifcativo
var ri = 0, rf = 0, ra = 0, posPa = 1, cl = null; //posicion inicial, final, actual de los registros, clase
var tiempo = null;
var reSel = null;
var titulom;
function seleccionLimites() {
    var i;
    limites[0] = parseInt(document.getElementById("se").value);
    document.getElementById("paginaN").value = 1;//Math.ceil(cuantos / limites[0]);
    seleccionPagina();

}
function seleccionPagina() {
    var auxposPa = posPa;
    posPa = parseInt(document.getElementById("paginaN").value);
    if (Math.ceil(cuantos / limites[0]) >= posPa && (posPa>0)) {
        document.getElementById("paginaN").max = Math.ceil(cuantos / limites[0]);//maximo valor permitido
        document.getElementById("paginaN").min="1";
        rf = posPa * parseInt(limites[0]);
        ri = rf - parseInt(limites[0]);
        i = document.getElementById("ctabla");
        i.parentNode.removeChild(i);
        parrilla.reStart();
    } else {
        document.getElementById("paginaN").value = auxposPa;
        posPa= auxposPa;
    }
}
function enMouse(e) {
    if (document.getElementById(e).style.backgroundColor == 'rgb(186, 209, 236)') {//'#bad1ec'
        document.getElementById(e).style.background = '#f6ecac';
    }
}
function saMouse(e) {
    if (document.getElementById(e).style.backgroundColor == 'rgb(246, 236, 172)') {//'#f6ecac'
        document.getElementById(e).style.background = '#bad1ec';
    }
}

function unClick(filaId) {
    if (!tiempo) {
        tiempo = setTimeout(function () {
            tiempo = null;
            if (reSel == null) {
                document.getElementById(filaId).style.background = '#CD853F';
                reSel = filaId;
            } else {
                if (filaId == reSel) {
                    document.getElementById(filaId).style.background = '#bad1ec';
                    reSel = null;
                } else {
                    if (filaId != reSel) {
                        document.getElementById(reSel).style.background = '#bad1ec';
                        document.getElementById(filaId).style.background = '#CD853F';
                        reSel = filaId;
                    }
                }
            }
        }, 200);
    }
}
function dosClicks(filaId) {
    if (tiempo) {
        clearTimeout(tiempo);
        //alert("cancelado un click");
        tiempo = null;
    }
    window.location = "controlador.php?ctl=a" + cl + "&sr=" + filaId;
}

function bot(c) {
    switch (c) {
        case "de":
            if (posPa < (Math.ceil(cuantos / limites[0]))) {
                ++posPa;
                document.getElementById("paginaN").value = posPa;
                rf = ((posPa * limites[0]) - 1);
                ri = ((posPa * limites[0]) - limites[0]);
                i = document.getElementById("ctabla");
                i.parentNode.removeChild(i);
                parrilla.reStart();
            }
            break;
        case "finDe":
            if (posPa < (Math.ceil(cuantos / limites[0]))) {
                posPa = Math.ceil(cuantos / limites[0]);
                document.getElementById("paginaN").value = posPa;
                rf = (posPa * limites[0]); //igual hay que incrementar ri en uno
                ri = (rf - parseInt(limites[0]));
                i = document.getElementById("ctabla");
                i.parentNode.removeChild(i);
                parrilla.reStart();
            }
            break;
        case "iz":
            if (posPa > 1) {
                --posPa;
                document.getElementById("paginaN").value = posPa;
                rf = (posPa * limites[0]);
                ri = ((posPa * limites[0]) - limites[0]);
                i = document.getElementById("ctabla");
                i.parentNode.removeChild(i);
                parrilla.reStart();
            }
            break;
        case "finIz":
            if (posPa > 1) {
                posPa = 1;
                document.getElementById("paginaN").value = posPa;
                ri = 0; //igual hay que incrementar ri en uno
                rf = (ri + limites[0]);
                i = document.getElementById("ctabla");
                i.parentNode.removeChild(i);
                parrilla.reStart();
            }
            break;
        case "mas":
            window.location = "controlador.php?ctl=a" + cl;
            break;
        case "menos":
            if (reSel) {
                confirmar = confirm("Desea borrar el registro seleccionado?");
                if (confirmar) {
                    window.location = "controlador.php?ctl=e" + cl + "&sr=" + reSel;
                }
            }
            break;
    }
}

function Parrilla() {

    this.Start = function (m, u, l, c) {//clase, registros x pantalla, campos
        limites = l;
        cl = u;
        rf = ri + parseInt(limites[0]);
        i = 0;
        titulom = m;
        for (i = 0; i < c.length; i++) {
            columnas[i] = c[i][1];
            mcampos[i] = c[i][0];
            if (c[i][3] == 1) {
                campos[i] = c[i][0];//cojemos solos los campos que necesitamos para hacer la consulta de SQL
            }
            if (c[i][2] == 1) {
                campoId = c[i][0];
            }
        }
        this.reStart();
    }

    this.reStart = function () {
//comprobar si existe la tabla creada y borrar para asi aligerar el codigo de arriba, http://jorgeortizperis.hol.es/controlador/
       // var url = "http://jorgeortizperis.hol.es/controlador/C_parrilla.php";
        var url = "http://localhost/proyectoFinalModulo/controlador/C_parrilla.php";
        var procesaError = null;
        var procesaCarga = null;
        var metodo = "POST";
        var parametros = "ri=" + ri + "&rf=" + limites[0] + "&ca=" + campos + "&me=" + cl; // pasamos la posicion inicial y cuantos registros a leer
        //alert(parametros);
        var contentType = "application/x-www-form-urlencoded";
        var procesaCaducidad = null;
        var caducidad = null;
        var cargador = new miAjax.CargadorContenidos(
                url,
                this.ProcesaRespuesta,
                procesaError,
                procesaCarga,
                metodo,
                parametros,
                contentType,
                procesaCaducidad,
                caducidad);
    };

    this.ProcesaRespuesta = function () {
// Obtenemos la respuesta del metodo miAjax, utilizo el metodo Json
        var respuesta_json = this.peticion.responseText; //respuesta del servidor
        //alert(respuesta_json); // cuando da un error activar este alert              
        var res_1 = eval("(" + respuesta_json + ")"); //convierto texto a json
        cuantos = res_1.cuantos[0].total;
        var respuesta = res_1.registros;
        //var respuesta = JSON.parse(respuesta_json);// version mejorada del eval.        
        if (respuesta) {
            dibujaParrilla(respuesta);
        }
    };
}
function dibujaParrilla(respuesta) {
    var i, j, l;
    var fila, celda, divis, inpu, se, opc;
    var nuevo;
    //if (respuesta.length > 0) {
    //comprobamos si ya existe la tabla creada e insertamos los elementos en la tabla        
    nuevo = (document.getElementById("tabla")) ? false : true;
    var elementoTabla = document.getElementById("tabla") ? document.getElementById("tabla") : document.getElementById("table");
    if (nuevo) {
        var tabla = document.createElement("TABLE"); // toda la tabla
        tabla.className = "parrilla";
        var cabecera = document.createElement("THEAD"); // la cabecera
        cabecera.className = "parrilla";
        var titulo = document.createElement("CAPTION"); // titulo por encima de la cabecera
        tabla.border = 1;
        tabla.appendChild(titulo);
        // Insertar una fila en la cabecera y configurar el color de fondo.
        fila = document.createElement("TR");
        cabecera.setAttribute("bgColor", "green");
        cabecera.appendChild(fila);
        // Crear e insertar celdas en la fila de la cabecera.
        for (i = 0; i < columnas.length; i++) {
            celda = document.createElement("TH");
            celda.className = "parrilla";
            celda.innerHTML = columnas[i];
            fila.appendChild(celda);
        }
        tabla.appendChild(cabecera);
    }

// Insertamos las filas y celdas en el cuerpo.
    var cuerpo = document.createElement("TBODY"); //el cuerpo
    if (respuesta.length > 0) {
        for (i = 0; i < respuesta.length; i++) {
            fila = document.createElement("TR");
            fila.id = eval('respuesta[i].' + campoId); //identificamos cada linea con un ID
            fila.style.background = "#bad1ec";
            cuerpo.appendChild(fila);
            fila.addEventListener("mouseover", function () {
                enMouse(this.id);
            }, false);
            fila.addEventListener("mouseout", function () {
                saMouse(this.id);
            }, false);
            fila.addEventListener("dblclick", function () {
                dosClicks(this.id);
            }, false);
            fila.addEventListener("click", function () {
                unClick(this.id);
            }, false);
            for (j = 0; j < columnas.length; j++) {
                celda = document.createElement("TD");
                celda.innerHTML = eval('respuesta[i].' + mcampos[j]);
                celda.className = "parrilla";
                fila.appendChild(celda);
            }
        }
    } else {
        fila = document.createElement("TR");
        cuerpo.appendChild(fila);
    }

    nuevo ? tabla.appendChild(cuerpo) : elementoTabla.appendChild(cuerpo);
    cuerpo.id = "ctabla";
    if (nuevo) {
        var pie = document.createElement("TFOOT"); //el pie
        pie.className = "parrilla";
        // Crear e insertar fila y celdas en el la fila del pie.
        fila = document.createElement("TR");
        pie.appendChild(fila);
        celda = document.createElement("TD");
        celda.className = "pieTabla parrilla";
        fila.style.background = "green";
        fila.appendChild(celda);
        //igual queda mejor metiendo este select dentro de un div
        se = document.createElement("SELECT");
        se.id = "se";
        se.className = "parrilla";
        celda.appendChild(se);
        se.addEventListener("change", function () {
            seleccionLimites();
        }, false);
        celda.colSpan = columnas.length;
        for (i = 1; i < limites.length; i++) {// ojo a partir de 1, el cero estan el numero de registros actuales
            opc = document.createElement("OPTION");
            opc.value = limites[i];
            opc.text = limites[i];
            if (limites[0] == limites[i])
                opc.selected = true;
            se.appendChild(opc);
        }
        for (i = 0; i < boton.length; i++) {
            divis = document.createElement("A");
            divis.id = boton[i];
            divis.className = boton[i];
            divis.setAttribute("href", "#");
            celda.appendChild(divis);
            divis.addEventListener("click", function () {
                bot(this.id);
            }, false);
        }

        divis = document.createElement("DIV"); //para crear los divs
        divis.id = "pag";
        divis.innerHTML = "Pagina:&nbsp;&nbsp;";
        inpu = document.createElement("INPUT");
        inpu.id = "paginaN";
        inpu.className = "parrilla";
        inpu.setAttribute("value", posPa);
        inpu.setAttribute("type", "number");
        inpu.setAttribute('min', '1');
        inpu.setAttribute('max', '' + Math.ceil(cuantos / limites[0])) + '"';
        divis.appendChild(inpu);
        inpu.addEventListener("change", function () {
            seleccionPagina();
        }, false);
        celda.appendChild(divis);
        // Configurar el titulo de la tabla.
        titulo.innerHTML = "<h4>" + titulom + "</h4>";
        tabla.appendChild(pie);
        tabla.id = "tabla";
        // Insertamos la tabla en la estructura.
        elementoTabla.appendChild(tabla);
    }
}
