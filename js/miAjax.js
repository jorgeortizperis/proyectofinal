//creo objeto
var miAjax = new Object();

// xx Creación de variables para los valores 0,1,2,3,4
miAjax.READY_STATE_UNINITIALIZED = 0;
miAjax.READY_STATE_LOADING = 1;
miAjax.READY_STATE_LOADED = 2;
miAjax.READY_STATE_INTERACTIVE = 3;
miAjax.READY_STATE_COMPLETE = 4;


// Creación de constructor del objeto miAjax.CargadorContenidos 
// llamare a CargadorContenidos pasandole todo lo necesrio para hacer una peticion ajax
miAjax.CargadorContenidos = function (
        url, procesaRespuesta, procesaError,
        procesaCarga, metodo, parametros, contentType,
        procesaCaducidad, caducidad) {

    //xx Aqui tendreis que rellenar: establecer propiedades del objeto generico creado
    this.url = url; // url del recurso
    this.procesaRespuesta = procesaRespuesta; //metodo a ejecutar cuando respuesta ok (resadystate= 4 y status=200 juntos)
    this.procesaError = (procesaError) ? procesaError : this.defaultError; //metodo a ejectuar cuando error
    this.procesaCarga = procesaCarga; //metodo a ejecutar mientras se carga la peticion (readystate = 1)
    this.procesaCaducidad = procesaCaducidad; // metodo que cancela la peticion tras un tiempo
    this.caducidad = caducidad; // tiempo de espepra hasta cancelar la peticion
    this.timer = -1; // será temporizador usado por prcesaCaducidad
    this.peticion = this.creaPeticion(); // peticion es el objeto XMLHttpRequest

    //cargaContenidos: hace la peticion. url/metodo para el open(), contentType si POST, parametros para send()
    this.cargaContenidos(
            url,
            metodo,
            parametros,
            contentType);
};

// creacion de metodos (creaPeticion, cargaContenidos, onReadyState, defaultError) 
// del objeto y meterlos al objeto con prototype
miAjax.CargadorContenidos.prototype =
{
    // 1) crear objeto xmlhttprequest para ie o el resto
    creaPeticion: function ()
    {
        if (window.XMLHttpRequest)
            return new XMLHttpRequest();
        else if (window.ActiveXObject)
            return new ActiveXObject("Microsoft.XMLHTTP");
    },
    // 2) y 3) Hace la peticion: prepara funcion respuesta y realiza la peticion, 
    cargaContenidos: function (url, metodo, parametros, contentType)
    {
        //this es el objeto miAjax. Si ha creado peticion (es el objeto XMLHttpRequest)
        if (this.peticion)
        {
            try
            {
                var loader = this; //loader es ahora miAjax
                /* loader = this: Es necesario almacenar una instancia del objeto this 
                 * porque desde una funcion anonima hay un fallo del lenguaje javascript 
                 * que no permite hacer referencia con this al objeto que contiene dicha 
                 * funcion anonima pero si que se puede hacer referencia a una variable 
                 * de su ambito, es decir, la instancia de this (XMLHttpRequest) pero
                 * esta vez almacenada en la variable peticion.*/

                // 2) prepara la funcion de respuesta
                this.peticion.onreadystatechange = function ()
                {
                    //usar funcion onReadyState (ver mas abajo) desde loader, es decir desde miAjax
                    loader.onReadyState.call(loader);
                };

                // 3) realizar la peticion: establecer parametros
                this.peticion.open(metodo, url, true);

                if (contentType) //si establezco un contenttype (para POST)
                {
                    this.peticion.setRequestHeader("Content-Type", contentType);
                }

                // 3) realizar la peticion: ejecutar peticion
                this.peticion.send(parametros);


                if (this.procesaCaducidad) // si se ha establecido este metodo
                {
                    this.timer = setTimeout(
                            function ()
                            {
                                loader.peticion.abort(); //cancela la peticion
                                loader.procesaCaducidad.call(loader); // usar metodo desde loader
                            },
                            this.caducidad);
                }
            } catch (err)
            {
                this.procesaError.call(this); //usar procesaError desde 
            }
        }
    },
    // 4) Decide que función hay que ejecutar cada vez que se dispara el evento readyState (cada
    // vez que se envía peticion al servidor), según el tipo de respueta obtenida
    onReadyState: function ()
    {
        //this es miAjax, peticion es XMLHttpRequest
        if (this.peticion.readyState == miAjax.READY_STATE_COMPLETE) //si estado = 4
        {
            if (this.peticion.status == 200 || this.peticion.status == 0)
            {
                clearTimeout(this.timer);
                if (this.peticion.status == 200)
                    // 4) llama a la funcion que establecimos para proceso de la respuesta
                        //alert (this.peticion.responseText); //en caso de erro activar este error para ver que devuelve
                    this.procesaRespuesta.call(this);
            }
            else
            {
                this.procesaError.call(this);
            }
        }
        else
        {
            if (this.peticion.readyState == miAjax.READY_STATE_LOADING) // si estado = 1
            {
                if (this.procesaCarga)
                    this.procesaCarga.call(this);
            }

        }
    },
    //cuando se produzca un error y no se establezca otro metodo para procesarlo
    defaultError: function ()
    {
        alert("Se ha producido un error en la petición Ajax al servidor!"
                + "\n\nreadyState:" + this.peticion.readyState
                + "\nstatus: " + this.peticion.status
                + "\nheaders: " + this.peticion.getAllResponseHeaders());
    }
};