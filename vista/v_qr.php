<?php
if (!isset($_SESSION)) {
    session_start();
    //header('location:../login.html');
}
if (empty($_SESSION['nombre'])) {
    if (!isset($_COOKIE['ot'])) {
        header('location:../login.html');
    } else {
        header('Location: controlador.php?ctl=login');
    }
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title>Logística Frigorífica - SETAFRI</title>

        <!-- CSS  -->
        <link href="/proyectoFinalModulo/css/materialize.css"  type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="/proyectoFinalModulo/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="/proyectoFinalModulo/css/estilos.css" type="text/css" rel="stylesheet" media="screen,projection"/>

        <!--  Scripts-->
        <script type="text/javascript" src="/proyectoFinalModulo/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/miAjax.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/funcionesParrilla.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/funcionesJava.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/materialize.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/init.js"></script>
    </head>
    <body>
        <nav class="light-green accent-4" role="navigation">
            <div class="nav-wrapper container">
                <ul class="left hide-on-med-and-down">                 
                    <li><a href=" <?php echo 'controlador.php?ctl=cerrar' ?>">Logout</a></li>
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
                <ul id="nav-mobile" class="side-nav">
                    <li><a href=" <?php echo 'controlador.php?ctl=cerrar' ?>">Logout</a></li>
                </ul>                   
            </div>
        </nav>
        <div class="container">
            <div class="col s12 m6 right-align">
                <h6>Bienvenido <?php echo $_SESSION['nombre'] ?></h6>
            </div>
            <div class="section">
                <div class="row">
                    <div class="col s12 m12">
                        <div class="row">
                            <form class="col s12" id="formulario" method="POST" action="controlador.php?ctl=movil&sr=<?php echo $expe->getidExpedicion() ?>">
                                <div class="col s12">
                                    <div class="row">
                                        <div class="input-field">
                                            <i class="mdi-action-account-circle prefix"></i>
                                            <input type="text" name="destinatario" id="destinatario"  readonly="" value="<?php echo $expe->getdestinatario() ?>"/>
                                            <label for="destinatario">Destinatario </label>   
                                        </div>
                                        <div class="input-field col m9 s12 right-align">
                                            <input type="radio" name="estado" id="estado" value="entregada" <?php
                                            if ($expe->getestado() == "entregada") {
                                                echo ('checked="true"');
                                            }
                                            ?>/>
                                            <label for="estado">Entregada</label>
                                            <input type="radio" name="estado" id="estado1" value="en reparto" <?php
                                            if ($expe->getestado() == "en reparto") {
                                                echo ('checked="true"');
                                            }
                                            ?>/>
                                            <label for="estado1">En reparto</label>
                                            <input type="radio" name="estado" id="estado2" value="incidencia" <?php
                                            if ($expe->getestado() == "incidencia") {
                                                echo ('checked="true"');
                                            }
                                            ?>/>
                                            <label for="estado2">Incidencia</label>
                                            <input type="radio" name="estado" id="estado3" value="en plaza destino" <?php
                                            if ($expe->getestado() == "en plaza destino") {
                                                echo ('checked="true"');
                                            }
                                            ?>/>
                                            <label for="estado3">Plaza destino</label>
                                        </div>
                                        <input type="hidden" name="idExpedicion" id="idExpedicion"  readonly="" value="<?php echo $expe->getidExpedicion() ?>"/>
                                        <div class="row">
                                            <div class="col m4 s12 offset-m3">
                                                <label class="black-text">Firma: </label> 
                                                <div name ="lienzo" id ="lienzo" class= "card-panel blue-grey lighten-5" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="input-field">
                                            <i class="mdi-editor-mode-edit prefix"></i>
                                            <textarea name="descripcionIncidencia" id="descripcionIncidencia" class="materialize-textarea"></textarea>
                                            <label for="descripcionIncidencia">Incidencia </label>   
                                        </div>
                                        <div class="input-field">
                                            <i class="mdi-editor-mode-edit prefix"></i>
                                            <textarea name="solucionIncidencia" id="solucionIncidencia" class="materialize-textarea" readonly=""><?php echo $expe->getsolucionIncidencia() ?></textarea>
                                            <label for="solucionIncidencia">Solución a la incidencia </label>
                                        </div>   
                                    </div>
                                    <div class="row">
                                        <div class="col m6 s12 center">
                                            <button class="btn waves-effect waves-light" type="submit" value="Aceptar" name="action">Aceptar
                                                <i class="mdi-content-send right"></i>
                                            </button>
                                        </div>
                                        <div class="section col s12 center hide-on-med-and-up"><p></p></div>


                                        <div class="col m6 s12 center">
                                            <button class="btn waves-effect waves-light" type="button" value="Cancelar" name="reset" onclick="location.replace('controlador.php?ctl=cerrar')" > 
                                                <i class="mdi-content-clear right"></i>Cancelar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>              
                </div>
            </div>
            <br><br>
        </div>
        <script>
            $(document).ready(function () {
                $('.button-collapse').sideNav();
                initCanvas();
            });
            var movimientos = new Array();
            var pulsado;

            function initCanvas() {
                var canvasDiv = document.getElementById('lienzo');
                canvas = document.createElement('canvas');
                canvas.setAttribute('width', 250);
                canvas.setAttribute('height', 210);
                canvas.setAttribute('id', 'canvas');
                canvasDiv.appendChild(canvas);
                if (typeof G_vmlCanvasManager != 'undefined') {
                    canvas = G_vmlCanvasManager.initElement(canvas);
                }
                context = canvas.getContext("2d");

                $('#canvas').mousedown(function (e) {
                    pulsado = true;
                    movimientos.push([e.pageX - this.offsetLeft,
                        e.pageY - this.offsetTop,
                        false]);
                    repinta();
                });
                $('#canvas').bind('touchstart', function (event) {// evento para pantalla movil, cuando se toca la pantalla
                    var e = event.originalEvent;
                    e.preventDefault();
                    pulsado = true;
                    movimientos.push([e.targetTouches[0].pageX - this.offsetLeft,
                        e.targetTouches[0].pageY - this.offsetTop,
                        false]);
                    repinta();
                });

                $('#canvas').mousemove(function (e) {
                    if (pulsado) {
                        movimientos.push([e.pageX - this.offsetLeft,
                            e.pageY - this.offsetTop,
                            true]);
                        repinta();
                    }
                });

                $('#canvas').bind('touchmove', function (event) {
                    var e = event.originalEvent;
                    e.preventDefault();
                    if (pulsado) {
                        movimientos.push([e.targetTouches[0].pageX - this.offsetLeft,
                            e.targetTouches[0].pageY - this.offsetTop,
                            true]);
                        repinta();
                    }
                });

                $('#canvas').mouseup(function (e) {
                    pulsado = false;
                });

                $('#canvas').bind('touchcancel', function (event) {//funcion para dispositivos moviles
                    pulsado = false;
                });
                $('#canvas').mouseleave(function (e) {
                    pulsado = false;
                });
                $('#canvas').bind('touchleave', function (event) {//funcion para dispositivos moviles
                    pulsado = false;
                });
                repinta();
            }
            function repinta() {
                canvas.width = canvas.width; // Limpia el lienzo

                context.strokeStyle = "#0000a0";//Color linea
                context.lineJoin = "round";
                context.lineWidth = 1;//diametro de la linea

                for (var i = 0; i < movimientos.length; i++)
                {
                    context.beginPath();
                    if (movimientos[i][2] && i) {
                        context.moveTo(movimientos[i - 1][0], movimientos[i - 1][1]);
                    } else {
                        context.moveTo(movimientos[i][0], movimientos[i][1]);
                    }
                    context.lineTo(movimientos[i][0], movimientos[i][1]);
                    context.closePath();
                    context.stroke();
                }
            }
            $("#formulario").submit(function (event) {

                var inci = document.getElementById('descripcionIncidencia').value;
                var iex = document.getElementById('idExpedicion').value;
                var est = "";
                var porEstado = document.getElementsByName("estado");
                for (var i = 0; i < porEstado.length; i++) {
                    if (porEstado[i].checked) {
                        est = porEstado[i].value;
                    }

                }
                $.post('../controlador/controlador.php?ctl=imagen',
                        {
                            img: canvas.toDataURL(), h: inci, e: est, f: iex
                        });
                alert("Firma guardada");
            });
        </script>

</html>