<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<?php ob_start() ?>
<div class="row">
    <div class="col s12">
        <form class="col s12" method="POST" action="controlador.php?ctl=aEnvios">
            <div class="row">
                <div class="col m3 s12">
                    <label for="fecha">Fecha</label>
                    <?php if ($expe->getfecha()) { ?>
                        <input type="date" name="fecha" id="fecha"  class= "datepicker" readonly="" value="<?php echo $expe->getfecha() ?>"/>
                    <?php } else { ?><input type="date" name="fecha" id="fecha"  class= "datepicker" value="<?php echo $expe->getfecha() ?>" required=""/>
                    <?php } ?>
                </div>  
                <div class="col m5 s12">
                    <label for="idCliente">Cod. Cliente</label>
                    <?php if ($expe->getidCliente()) { ?>
                        <?php foreach ($cli as $k) { ?>
                            <?php
                            if ($expe->getidCliente() == $k['cif_cliente']) {
                                $razon = $k['razonSocial'];
                            }
                        }
                        ?> 
                        <input type="text" value="<?php echo($razon); ?>" readonly=""/>
                        <input type="text" name="idCliente" id="idCliente" value="<?php echo($expe->getidCliente()); ?>" hidden=""/>
                    <?php } else { ?>
                        <select name="idCliente" id="idCliente" class="browser-default CodigoCliente" readonly="">
                            <?php if (!$expe->getidCliente()) { ?>
                                <option value="" disabled selected>Elige tu opción</option>
                            <?php } ?>
                            <?php foreach ($cli as $k) { ?> {
                                <?php if ($expe->getidCliente() == $k['cif_cliente']) { ?>
                                    <option value="<?php echo($k['cif_cliente']) ?>" selected><?php echo($k['razonSocial']); ?></option>
                                <?php } ?>              
                                <option value="<?php echo($k['cif_cliente']) ?>"><?php echo($k['razonSocial']); ?></option>
                            <?php } ?>                       
                        </select>
                    <?php } ?>  
                </div> 
                <div class="col m4 s12">
                    <label for="idExpedicion">Expedicion</label>
                    <?php if ($expe->getidExpedicion()) { ?>
                        <input type="text" name="idExpedicion" id="idExpedicion" readonly=" "value="<?php echo $expe->getidExpedicion() ?>"/>
                    <?php } else { ?><input type="text" name="idExpedicion" id="idExpedicion" readonly=""/>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col m5 s12">
                    <label for="idProvinciaOrigen">Provincia Origen</label>
                    <select name="idProvinciaOrigen" id="idProvinciaOrigen" class="browser-default" required="">
                        <?php if (!$expe->getidProvinciaOrigen()) { ?>
                            <option value="" disabled selected>Elige tu opción</option>
                        <?php } ?>
                        <?php foreach ($pro as $k) { ?> {
                            <?php if ($expe->getidProvinciaOrigen() == $k['IdProvincia']) { ?>
                                <option value="<?php echo($k['IdProvincia']) ?>" selected><?php echo($k['descripcion']); ?></option>
                            <?php } ?>              
                            <option value="<?php echo($k['IdProvincia']) ?>"><?php echo($k['descripcion']); ?></option>
                        <?php } ?>
                    </select>
                </div> 
                <div class="input-field col m7 s12">
                    <?php if ($expe->getdestinatario()) { ?>
                        <input type="text" name="destinatario" id="destinatario"  readonly="" value="<?php echo $expe->getdestinatario() ?>"/>
                    <?php } else { ?>                   
                        <input type="text" name="destinatario" id="destinatario" value="<?php echo $expe->getdestinatario() ?>" required=""/>
                        <label for="destinatario">Destinatario</label>
                    <?php } ?>


                </div> 
            </div>
            <div class="row">
                <div class="col m5 s12">
                    <label for="idProvinciaDestino">Provincia Destino</label>
                    <select name="idProvinciaDestino" id="idProvinciaDestino" class="browser-default ProvinciaDestino" required="">
                        <?php if (!$expe->getidProvinciaDestino()) { ?>
                            <option value="" disabled selected>Elige tu opción</option>
                        <?php } ?>
                        <?php foreach ($pro as $k) { ?> {
                            <?php if ($expe->getidProvinciaDestino() == $k['IdProvincia']) { ?>
                                <option value="<?php echo($k['IdProvincia']) ?>" selected><?php echo($k['descripcion']); ?></option>
                            <?php } ?>              
                            <option value="<?php echo($k['IdProvincia']) ?>"><?php echo($k['descripcion']); ?></option>
                        <?php } ?>
                    </select>
                </div> 
                <label for="TransportistaD">Transportista</label>
                <div class="input-field col m7 s12">
                    <input type="text" name="idTransportista" id="idTransportista"  hidden value="<?php
                    if ($expe->getidTransportista()) {
                        echo ($expe->getidTransportista());
                    }
                    ?>"/>
                    <input type="text" name="TransportistaD" id="TransportistaD"  readonly value="<?php
                    if ($expe->getidTransportista()) {
                        echo ($tra->getrazonSocial());
                    }
                    ?>"/>
                </div> 
            </div>

            <div class="row">
                <div class="input-field col m2 s12">
                    <input type="number" name="bultos" id="bultos" step="1" required="" value="<?php
                    if ($expe->getbultos()) {
                        echo ($expe->getbultos());
                    } else {
                        echo ("0");
                    }
                    ?>"/>
                    <label for="bultos">Bultos</label>
                </div> 
                <div class="input-field col m2 s12">
                    <input type="number" name="peso" id="peso"  step="0.001" value="<?php
                    if ($expe->getpeso()) {
                        echo ($expe->getpeso());
                    } else {
                        echo ("0");
                    }
                    ?>"/>
                    <label for="peso">Peso</label>
                </div> 
                <div class="input-field col m7 s12 right-align">
                    <input type="radio" name="estado" id="estado" value="entregada" <?php
                    if ($expe->getestado() == "entregada") {
                        echo ('checked="true"');
                    }
                    ?>/>
                    <label for="estado">Entregada</label>
                    <input type="radio" name="estado" id="estado1" value="en reparto" <?php
                    if ($expe->getestado() == "en reparto") {
                        echo ('checked="true"');
                    }
                    ?>/>
                    <label for="estado1">En reparto</label>
                    <input type="radio" name="estado" id="estado2" value="incidencia" <?php
                    if ($expe->getestado() == "incidencia") {
                        echo ('checked="true"');
                    }
                    ?>/>
                    <label for="estado2">Incidencia</label>
                    <input type="radio" name="estado" id="estado3" value="en plaza destino" <?php
                    if ($expe->getestado() == "en plaza destino") {
                        echo ('checked="true"');
                    }
                    ?>/>
                    <label for="estado3">Plaza destino</label>
                </div> 
            </div>
            <div class="row">
                <div class="input-field">
                    <i class="mdi-editor-mode-edit prefix"></i>
                    <textarea name="descripcionIncidencia" id="descripcionIncidencia" class="materialize-textarea"><?php
                    if ($expe->getdescripcionIncidencia()) {
                        echo ($expe->getdescripcionIncidencia());
                    }
                    ?></textarea>
                    <label for="descripcionIncidencia">Incidencia </label>   
                </div>

                <div class="input-field">
                    <i class="mdi-editor-mode-edit prefix"></i>
                    <textarea name="solucionIncidencia" id="solucionIncidencia" class="materialize-textarea"><?php
                    if ($expe->getsolucionIncidencia()) {
                        echo ($expe->getsolucionIncidencia());
                    }
                    ?></textarea>
                    <label for="solucionIncidencia">Solución a la incidencia </label>
                </div>
            </div>
            <div class="row">
                <div>
                    <i class="mdi-editor-mode-edit prefix"></i>
                    <img name ="imagen"<?php
                    if ($expe->getfirma()) {
                        echo (' src="data:image/png;base64,'.$expe->getfirma().'"');
                    }
                    ?>>
                    <label for="imagen">Firma: </label>
                </div>  
            </div>


            <div class="row">
                <div class="col m6 s12 center">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Aceptar
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
                <div class="section col s12 center hide-on-med-and-up"><p></p></div>
                <div class="col m6 s12 center">
                    <button class="btn waves-effect waves-light" type="reset" name="reset" onclick="location.replace('controlador.php?ctl=expedicion')" > 
                        <i class="mdi-content-clear right"></i>Cancelar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
$contenidoCuerpo = ob_get_clean();
$contenidoParrilla = "";
$contenidoJavasCript = " $('.datepicker').pickadate();
        $('.CodigoCliente').change(function() {ActualizarCliente(document.getElementById('idCliente').value);});
        $('.ProvinciaDestino').change(function() {MostarTransportista(document.getElementById('idProvinciaDestino').value);});
        $('.ProvinciaDestino').blur(function() {ComprobarDestino(document.getElementById('idProvinciaDestino').value);});";
require_once config::sitio() . '/proyectoFinalModulo/vista/page.php';
//require_once config::sitio() . '/vista/page.php';
?>