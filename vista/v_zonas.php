<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<?php ob_start() ?>
<div class="col m6 s12 offset-m3">
    <form class="col s12" method="POST" action="controlador.php?ctl=aPertenece">
        <div class="col s12">
            <label for="cif_transportista">C.I.F. Transportista</label>
            <select name="cif_transportista" id="cif_transportista" class="browser-default selectProvincia">
                <?php if (!$zo->getcifTransportista()) { ?>
                    <option value="" disabled selected>Choose your option</option>
                <?php } ?>
                <?php foreach ($tra as $k) { ?> {
                    <?php if ($zo->getcifTransportista() == $k['cif_transportista']) { ?>
                        <option value="<?php echo($k['cif_transportista']) ?>" selected><?php echo($k['razonSocial']); ?></option>
                    <?php } ?>              
                    <option value="<?php echo($k['cif_transportista']) ?>"><?php echo($k['razonSocial']); ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col s12">
            <label for="provincia">Provincia</label>
            <select name="provincia" id="provincia" class="browser-default">
                <?php if (!$zo->getprovincia()) { ?>
                    <option value="" disabled selected>Choose your option</option>
                <?php } ?>
                <?php foreach ($pro as $k) { ?> {
                    <?php if ($zo->getprovincia() == $k['IdProvincia']) { ?>
                        <option value="<?php echo($k['IdProvincia']) ?>" selected><?php echo($k['descripcion']); ?></option>
                    <?php } ?>              
                    <option value="<?php echo($k['IdProvincia']) ?>"><?php echo($k['descripcion']); ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="input-field col  s12">
            <label for="costeKm"> Coste por Km. </label>
            <input type="number" name="costeKm" id="costeKm" step="0.001" value="<?php echo $zo->getcosteKm() ?>" required />
        </div> 
        <div class="row">
        </div>
        <div class="row">
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Aceptar
                    <i class="mdi-content-send right"></i>
                </button>
            </div>
            <div class="section col s12 center hide-on-med-and-up"><p></p></div>
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="reset" name="reset" onclick="location.replace('controlador.php?ctl=zona')" > 
                    <i class="mdi-content-clear right"></i>Cancelar
                </button>
            </div>
        </div>
    </form>
</div>
<?php
$contenidoCuerpo = ob_get_clean();
$contenidoParrilla = "";

$contenidoJavasCript = "$('.selectProvincia').change(function() {Actualizar(document.getElementById('cif_transportista').value);});";
require_once config::sitio() . '/proyectoFinalModulo/vista/page.php';
?>