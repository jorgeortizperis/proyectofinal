<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<?php ob_start() ?>
<div class="col m12 s12 offset-m3">
    <form class="col m6 s12" method="POST" action="controlador.php?ctl=aClientes">
        <div class="input-field col  s12">
            <label for="nif">C.I.F. Cliente</label>
            <?php if ($cli->getcifCliente()) { ?>
                <input type="text" readonly="readonly" id ="cif_cliente" name="cif_cliente" value="<?php echo $cli->getcifCliente() ?>" />
            <?php } else { ?><input type="text" id ="cif_cliente" name="cif_cliente" pattern="^[A-Z][0-9]{8}$" value="<?php echo $cli->getcifCliente() ?>" required />
            <?php } ?>
        </div>          
        <div class="input-field col  s12">
            <label for="descripcion"> Razón social </label>
            <input type="text" name="razonSocial" id="descripcion" value="<?php echo $cli->getrazonSocial() ?>" required />
        </div>        
        <div class="row">
        </div>
        <div class="row">
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Aceptar
                    <i class="mdi-content-send right"></i>
                </button>
            </div>
            <div class="section col s12 center hide-on-med-and-up"><p></p></div>
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="reset" name="reset" onclick="location.replace('controlador.php?ctl=cliente')" > 
                    <i class="mdi-content-clear right"></i>Cancelar
                </button>
            </div>
        </div>
    </form>
</div>
<?php
$contenidoCuerpo = ob_get_clean();
$contenidoParrilla = "";
    $contenidoJavasCript = '$("#cif_cliente").change(function () {                  
                    if (document.getElementById("cif_cliente").value) {
                        var de = document.getElementById("cif_cliente").value;
                        var url = "./controlador.php?ctl=comprobarDniC"; // El script a dónde se realizará la petición.
                        $.post(url, {f:de}, function (data) {
                        if (data){alert ("CIF ya registrado, introduzca otro.")
                        document.getElementById("cif_cliente").value="";
                        document.getElementById("cif_cliente").focus();
                        }    
                        });
                    }
                });';
require_once config::sitio() . '/proyectoFinalModulo/vista/page.php';
?>