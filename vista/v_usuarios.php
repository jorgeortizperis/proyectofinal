<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<?php ob_start() ?>

<div class="col m6 s12 offset-m3">
    <form class="col s12" method="POST" action="controlador.php?ctl=aUsuarios">
        <div class="input-field col s12">
            <label for="dni">D.N.I.</label>
            <?php if ($usu->getdni()) { ?>
                <input type="text" readonly="readonly" name="dni" id="dni" value="<?php echo $usu->getdni() ?>" />
            <?php } else { ?><input type="text" name="dni" id="dni"  pattern="^[0-9]{8}[A-Z]$" value="<?php echo $usu->getdni() ?>" required />
            <?php } ?>
        </div> 

        <div class="input-field col s12">
            <label for="nombre">Nombre y Apellidos</label>
            <input type="text" name="nombre" id="nombre" value="<?php echo $usu->getnombre() ?>" required />
        </div> 
        <div class="input-field col s12">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" value="<?php echo $usu->getemail() ?>" required />
        </div>
        <div class="input-field col s12">
            <label for="pass">Contraseña</label>
            <input type="password" name="pass" id="pass" required/>
        </div>
        <input type="hidden" id="cli" name="cli" value="<?php if (!empty($usu->getncifCliente())) echo $usu->getncifCliente(); ?>" />
        <input type="hidden" id="tra" name="tra" value="<?php if (!empty($usu->getcifTransportista())) echo $usu->getcifTransportista(); ?>" />
        <input type="hidden" id="ni" name="ni" value="<?php echo $usu->getnivelSeguridad() ?>" />

        <?php if ($_SESSION['idRol'] == 1) { ?>
            <label for="idRol">Rol</label>
            <select name="idRol" id="idRol" class="browser-default">
                <?php if (!$usu->getnivelSeguridad()) { ?>
                    <option value="" disabled selected>Selecciona una opción</option>
                    <?php foreach ($rol as $k) { ?> 
                        <?php if ($usu->getnivelSeguridad() == $k['idRol']) { ?>
                            <option value="<?php echo($k['idRol']) ?>" selected><?php echo($k['descripcion']); ?></option>
                        <?php } ?> 
                        <?php if ($k['idRol'] <= 3) { ?>    
                            <option value="<?php echo($k['idRol']) ?>"><?php echo($k['descripcion']); ?></option>
                        <?php } ?>
                        <?php
                    }
                } else {
                    ?>
                    <?php foreach ($rol as $k) { ?> {
                        <?php if ($usu->getnivelSeguridad() == $k['idRol']) { ?>
                            <?php if ($k['idRol'] <= 3) { ?>  
                                <option value="<?php echo($k['idRol']) ?>" disabled selected><?php echo($k['descripcion']); ?></option>
                            <?php } ?>
                        <?php } ?>              
                        <?php
                    }
                }
                ?>
            </select>
            <div class="row">
                <label for="idQue">Cliente / Transportista:</label>
                <select name="idQue" id="idQue" class="browser-default idQue" style="display:none;">
                    <option value="" disabled selected>Elige tipo de usuario</option>
                    <option value="" selected></option>             
                </select>
            </div>
        <?php } ?>
        <div class="row">
        </div>
        <div class="row">
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Aceptar
                    <i class="mdi-content-send right"></i>
                </button>
            </div>
            <div class="section col s12 center hide-on-med-and-up"><p></p></div>
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="reset" name="reset" onclick="location.replace('controlador.php?ctl=usuario')" > 
                    <i class="mdi-content-clear right"></i>Cancelar
                </button>
            </div>
        </div>
    </form>
    <?php
    $contenidoCuerpo = ob_get_clean();
    $contenidoParrilla = "";
    $contenidoJavasCript = '$("#dni").change(function () {                  
                    if (document.getElementById("dni").value) {
                        var de = document.getElementById("dni").value;
                        var url = "./controlador.php?ctl=comprobarDniU"; // El script a dónde se realizará la petición.
                        $.post(url, {f:de}, function (data) {
                        if (data=="true"){alert ("DNI ya registrado, introduzca otro.")
                        document.getElementById("dni").value="";
                        document.getElementById("dni").focus();
                        }    
                        });
                    }
                });
                    $("#idRol").change(function () {
                    var po = document.getElementById("idRol").selectedIndex;
                    var de = document.getElementById("idRol").options[po].value;
                    if (de>=2){
                        document.getElementById("idQue").style.display="block";
                        var url = "./controlador.php?ctl=buscarTipoU";                      
                        $.post(url, {f: de}, function (data) {
                            document.getElementById("idQue").required;
                            $("#idQue").html(data);
                        });
                    }
                });
                $("#idQue").change(function (){
                    var po = document.getElementById("idRol").selectedIndex;
                    var de = document.getElementById("idRol").options[po].value;
                    if (de==2){
                        var poo = document.getElementById("idQue").selectedIndex;
                        var dee = document.getElementById("idQue").options[poo].value;
                        document.getElementById("cli").value=dee;
                    }
                    if (de==3){
                        var poo = document.getElementById("idQue").selectedIndex;
                        var dee = document.getElementById("idQue").options[poo].value;
                        document.getElementById("tra").value=dee;
                    }
                });';
    require config::sitio() . '/proyectoFinalModulo/vista/page.php';
    //require config::sitio() . '/vista/page.php';
    ?>