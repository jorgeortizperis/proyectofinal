<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<?php ob_start() ?>
<div class="col m6 s12 offset-m3">
    <form class="col s12" method="POST" action="controlador.php?ctl=aTransportistas">
        <div class="input-field col s12">
            <label for="cif_transportista">C.I.F. Transportista</label>
            <?php if ($tra->getcifTransportista()) { ?>
                <input type="text" readonly="readonly" id="cif_transportista" name="cif_transportista" value="<?php echo $tra->getcifTransportista() ?>" />
            <?php } else { ?><input type="text" id="cif_transportista" name="cif_transportista" pattern="^[A-Z][0-9]{8}$" value="<?php echo $tra->getcifTransportista() ?>" required />
            <?php } ?>
        </div> 
        <div class="input-field col s12">
            <label for="descripcion"> Razón social </label>
            <input type="text" name="razonSocial" id="razonSocial" value="<?php echo $tra->getrazonSocial() ?>" required />
        </div> 
        <div class="col s12">
            <label for="provincia">Provincia</label>
            <select name="provincia" id="provincia" class="browser-default">
                <?php if (!$tra->getprovincia()) { ?>
                    <option value="" disabled selected>Choose your option</option>
                <?php } ?>
                <?php foreach ($pro as $k) { ?> {
                    <?php if ($tra->getprovincia() == $k['IdProvincia']) { ?>
                        <option value="<?php echo($k['IdProvincia']) ?>" selected><?php echo($k['descripcion']); ?></option>
                    <?php } ?>              
                    <option value="<?php echo($k['IdProvincia']) ?>"><?php echo($k['descripcion']); ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="row">
        </div>
        <div class="row">
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="submit" name="action">Aceptar
                    <i class="mdi-content-send right"></i>
                </button>
            </div>
            <div class="section col s12 center hide-on-med-and-up"><p></p></div>
            <div class="col m6 s12 center">
                <button class="btn waves-effect waves-light" type="reset" name="reset" onclick="location.replace('controlador.php?ctl=transportista')" > 
                    <i class="mdi-content-clear right"></i>Cancelar
                </button>
            </div>
        </div>
    </form>
</div>
<?php
$contenidoCuerpo = ob_get_clean();
$contenidoParrilla = "";
    $contenidoJavasCript = '$("#cif_transportista").change(function () {                  
                    if (document.getElementById("cif_transportista").value) {
                        var de = document.getElementById("cif_transportista").value;
                        var url = "./controlador.php?ctl=comprobarDniT"; // El script a dónde se realizará la petición.
                        $.post(url, {f:de}, function (data) {
                        if (data){alert ("CIF ya registrado, introduzca otro.")
                        document.getElementById("cif_transportista").value="";
                        document.getElementById("cif_transportista").focus();
                        }    
                        });
                    }
                });';
require_once config::sitio() . '/proyectoFinalModulo/vista/page.php';
?>