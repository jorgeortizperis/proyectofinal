<?php
if (!isset($_SESSION)) {
    session_start();
}

if (empty($_SESSION['nombre'])) {
    header('location:../login.html');
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Logística Frigorífica - SETAFRI</title>

        <!-- CSS  -->
        <link href="/proyectoFinalModulo/css/materialize.css"  type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="/proyectoFinalModulo/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="/proyectoFinalModulo/css/estilos.css" type="text/css" rel="stylesheet" media="screen,projection"/>

        <!--  Scripts-->
        <script type="text/javascript" src="/proyectoFinalModulo/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/miAjax.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/funcionesParrilla.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/funcionesJava.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/materialize.js"></script>
        <script type="text/javascript" src="/proyectoFinalModulo/js/init.js"></script>
    </head>

    <body>
        <div class="section no-pad-bot" id="index-banner">
            <div class="container">
                <img src="/proyectoFinalModulo/images/Setafri_camiones.jpg" class="responsive-img">
            </div>
        </div>

        <nav class="light-green accent-4" role="navigation">
            <div class="nav-wrapper container">
                <ul class="left hide-on-med-and-down">
                    <?php foreach ($_SESSION['me'] as $item): ?>
                        <li><a href="<?php echo $item['m'] ?>"><?php echo $item['op'] ?></a></li>
                    <?php endforeach; ?>
                    <li><a href=" <?php echo 'controlador.php?ctl=cerrar' ?>">Logout</a></li>
                </ul>

                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
                <ul id="nav-mobile" class="side-nav">
                    <?php foreach ($menu as $item): ?>
                        <li><a href="<?php echo $item['m'] ?>"><?php echo $item['op'] ?></a></li>
                    <?php endforeach; ?>
                    <li><a href=" <?php echo 'controlador.php?ctl=cerrar' ?>">Logout</a></li>
                </ul>                   
            </div>
        </nav>


        <div class="container">

            <div class="col s12 m6 right-align">
                <h6>Bienvenido <?php echo $_SESSION['nombre'] ?></h6>
            </div>

            <div class="section">
                <div class="row">
                    <div class="col s12 m12">
                        <div class ="rejilla" id ="table">
                            <!--<button onclick="parrilla()">Click me</button> -->
                        </div>
                        <?php echo $contenidoCuerpo ?>
                    </div>              
                </div>
            </div>
            <br><br>
        </div>
        <script>
<?php echo $contenidoParrilla ?>
            $(document).ready(function () {
                $('.button-collapse').sideNav();
                $('select').material_select();
<?php echo $contenidoJavasCript ?>
            });

        </script>
    </body>
</html>
