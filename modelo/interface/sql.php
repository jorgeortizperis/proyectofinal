<?php

//echo getcwd() . "\n"; //imprime la posicion actual
require_once $_SERVER['DOCUMENT_ROOT'] . '/proyectoFinalModulo/config/config.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.php';
class mySql {

    public static function conectar() {
        try {
            Config::$mvc_bd_hostname;
            $conexion = new PDO('mysql:host=' . Config::$mvc_bd_hostname . ';dbname=' . Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave);
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conexion->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            print "<script><p>error al conectar!!</p></script>";
            print "<p>Error: No puede conectarse con la base de datos.</p>\n";
            exit();
        }
        return ($conexion);
    }

    public static function desconectar($conexion) {
        $conexion = null;
    }

    // * * * * * * *  R O L  * * * * * * * * * 
    public static function buscarRol() {
        $db = self::conectar();
        $re = $db->prepare('SELECT * FROM rol');
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    // * * * * * * *  P R O V I N C I A S  * * * * * * * * * 
    public static function buscarProvincia($e) {
        $db = self::conectar();
        if ($e == null) {
            $re = $db->prepare('SELECT * FROM provincias');
        } else {
            $re = $db->prepare('SELECT * FROM provincias WHERE IdProvincia NOT IN (SELECT idProvincia FROM pertenece)');
            $re->bindParam(':id', $e);
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function buscarProvinciaN($e) {
        $db = self::conectar();
        $re = $db->prepare('SELECT * FROM provincias WHERE IdProvincia =:id');
        $re->bindParam(':id', $e);
        $re->execute();
        $rows = $re->fetch(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($rows) {
            return new Provincia($rows["IdProvincia"], $rows["descripcion"]);
        } else {
            return false;
        }
    }

    // * * * * * * *  U S U A R I O S  * * * * * * * * * 
    public static function leerU() {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare('SELECT * FROM usuarios');
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function leerUsuarios($ri, $rf, $ca) {
        // obtengo todas las filas
        $db = self::conectar();
        if (isset($_SESSION)) {
            switch ($_SESSION['idRol']) {
                case 1:
                    $re = $db->prepare('SELECT ' . $ca . ' FROM usuarios limit ' . $ri . ',' . $rf);
                    break;
                case 2: //cliente
                    $re = $db->prepare('SELECT ' . $ca . ' FROM usuarios WHERE cif_cliente = :id limit ' . $ri . ',' . $rf);
                    $re->bindParam(':id', $_SESSION['cliente']);
                    break;
                case 3: //transportista
                    $re = $db->prepare('SELECT ' . $ca . ' FROM usuarios WHERE cif_transportista = :id limit ' . $ri . ',' . $rf);
                    $re->bindParam(':id', $_SESSION['transportista']);
                    break;
            }
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function contarUsuarios() {
        // obtengo todas las filas
        $db = self::conectar();
        if (isset($_SESSION)) {
            switch ($_SESSION['idRol']) {
                case 1:
                    $re = $db->prepare("SELECT COUNT(*) as total FROM usuarios");
                    break;
                case 2: //cliente
                    $re = $db->prepare("SELECT COUNT(*) as total FROM usuarios where cif_cliente=:id");
                    $re->bindParam(':id', $_SESSION['cliente']);
                    break;
                case 3: //transportista
                    $re = $db->prepare("SELECT COUNT(*) as total FROM usuarios where cif_transportista =:id");
                    $re->bindParam(':id', $_SESSION['transportista']);
                    break;
            }
        }

        //$re = $db->prepare("SELECT COUNT(*) as total FROM usuarios");
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function buscarUsuarios($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT * FROM usuarios WHERE dni = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($registro) {
            return new Usuario($registro['dni'], $registro['nombre'], $registro['email'], $registro['contrasena'], $registro['idRol'], $registro['cif_transportista'], $registro['cif_cliente']);
            //return new Usuario($registro[0]['dni'], $registro[0]['nombre'], $registro[0]['email'], $registro[0]['contrasena'], $registro[0]['idRol'], $registro[0]['cif_transportista'], $registro[0]['cif_cliente']);
        } else {
            return false;
        }
    }

    public static function buscarUsuariosTF($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT * FROM usuarios WHERE dni = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch();
        self::desconectar($db);
        if ($registro) {
            return true;
        } else {
            return false;
        }
    }

    public static function guardarUsuarios() {
        $db = self::conectar();
        if (self::buscarUsuariosTF(Usuario::getdni())) /* Modifica */ {
            $consulta = $db->prepare('UPDATE usuarios SET nombre = :nombre, email = :correo, contrasena = :pa WHERE dni = :dni');
            $consulta->bindParam(':dni', Usuario::getdni());
            $consulta->bindParam(':nombre', Usuario::getnombre());
            $consulta->bindParam(':correo', Usuario::getemail());
            $consulta->bindParam(':pa', Usuario::getpassword());
            //$consulta->bindParam(':rol', Usuario::getnivelSeguridad());
            //$consulta->bindParam(':tra', Usuario::getcifTransportista());
            //$consulta->bindParam(':cli', Usuario::getncifCliente());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al dar de alta un usuario</p></script>";
                print $e->getMessage();
                exit();
            }
        } else /* Inserta */ {
            //if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            //    echo "Wiii, tengo CRYPT_BLOWFISH!";
            //}
            // Al utilizar este metodo de encriptacion no se puede visualizar al usuario su contraseña almacenada, a no ser que se guarde la semilla.
            // Generamos un salt aleatoreo, de 22 caracteres para Bcrypt
            //$salt = substr(base64_encode(openssl_random_pseudo_bytes('30')), 0, 22);
            // A Crypt no le gustan los '+' así que los vamos a reemplazar por puntos.
            //$salt = strtr($salt, array('+' => '.'));
            // Generamos el hash
            //$hash = crypt(Usuario::getpassword(), '$2y$10$' . $salt);
            //$password_hash = crypt(Usuario::getpassword());
            $consulta = $db->prepare('INSERT INTO usuarios (dni, nombre, email, contrasena, idRol, cif_transportista, cif_cliente) VALUES(:dni, :nombre, :correo, :pa, :rol, :tra, :cli)');
            $consulta->bindParam(':dni', Usuario::getdni());
            $consulta->bindParam(':nombre', Usuario::getnombre());
            $consulta->bindParam(':correo', Usuario::getemail());
            $consulta->bindParam(':pa', Usuario::getpassword());
            //$consulta->bindParam(':passw', $hash);
            $consulta->bindParam(':rol', Usuario::getnivelSeguridad());
            $consulta->bindParam(':tra', Usuario::getcifTransportista());
            $consulta->bindParam(':cli', Usuario::getncifCliente());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al dar de alta un usuario</p></script>";
                print $e->getMessage();
                exit();
            }
            Usuario::setdni($db->lastInsertId());
        }
        self::desconectar($db);
    }

    public static function borrarUsuarios($u) {
        $db = self::conectar();
        $consulta = $db->prepare('DELETE FROM usuarios WHERE dni = :id');
        $consulta->bindParam(':id', Usuario::getdni());
        $consulta->execute();
        self::desconectar($db);
    }

    // * * * * * * *  C L I E N T E  * * * * * * * * * 

    public static function leerClientes($ri, $rf, $ca) {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare('SELECT ' . $ca . ' FROM cliente limit ' . $ri . ',' . $rf);
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function contarClientes() {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare("SELECT COUNT(*) as total FROM cliente");
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function buscarClientes($e) {
        $db = self::conectar();
        if ($e == null) {
            $re = $db->prepare('SELECT * FROM cliente');
        } else {
            $re = $db->prepare('SELECT * FROM cliente WHERE cif_cliente = :id');
            $re->bindParam(':id', $e);
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($rows) {
            if ($e == null) {
                return $rows;
            } else {
                return new Cliente($rows[0]["cif_cliente"], $rows[0]["razonSocial"]);
            }
        } else {
            return false;
        }
    }

    public static function buscarClientesTF($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT cif_cliente FROM cliente WHERE cif_cliente = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch();
        self::desconectar($db);
        if ($registro) {
            return true;
        } else {
            return false;
        }
    }

    public static function guardarClientes() {
        $db = self::conectar();
        if (self::buscarClientesTF(Cliente::getcifCliente())) /* Modifica */ {
            $consulta = $db->prepare('UPDATE cliente SET razonSocial = :nombre WHERE cif_cliente = :id');
            $consulta->bindParam(':nombre', Cliente::getrazonSocial());
            $consulta->bindParam(':id', Cliente::getcifCliente());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al modificar un cliente</p></script>";
                exit();
            }
        } else /* Inserta */ {
            $consulta = $db->prepare('INSERT INTO cliente (cif_cliente, razonSocial) VALUES(:id, :nombre)');
            $consulta->bindParam(':id', Cliente::getcifCliente());
            $consulta->bindParam(':nombre', Cliente::getrazonSocial());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al insertar un cliente</p></script>";
                exit();
            }
            Cliente::setcifCliente($db->lastInsertId());
        }
        self::desconectar($db);
    }

    public static function borrarCliente() {
        $db = self::conectar();
        $consulta = $db->prepare('DELETE FROM cliente WHERE cif_cliente = :id');
        $consulta->bindParam(':id', Cliente::getcifCliente());
        $consulta->execute();
        self::desconectar($db);
    }

    // * * * * * * *  T R A N S P O R T I S T A S  * * * * * * * * * 
    public static function leerTransportistas($ri, $rf, $ca) {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare('SELECT ' . $ca . ',(SELECT descripcion FROM provincias WHERE provincias.IdProvincia=transportistas.idProvincia) as descripcion FROM transportistas limit ' . $ri . ',' . $rf);
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function contarTransportistas() {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare("SELECT COUNT(*) as total FROM transportistas");
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function buscarTransportistas($e) {
        $db = self::conectar();
        if ($e == null) {
            $re = $db->prepare('SELECT * FROM transportistas');
        } else {
            $re = $db->prepare('SELECT * FROM transportistas WHERE cif_transportista = :id');
            $re->bindParam(':id', $e);
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($rows) {
            if ($e == null) {
                return $rows;
            } else {
                return new Transportista($rows[0]["cif_transportista"], $rows[0]["razonSocial"], $rows[0]["idProvincia"]);
            }
        } else {
            return false;
        }
    }

    public static function buscarTransportistasTF($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT cif_transportista FROM transportistas WHERE cif_transportista = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch();
        self::desconectar($db);
        if ($registro) {
            return true;
        } else {
            return false;
        }
    }

    public static function guardarTransportistas() {
        $db = self::conectar();
        if (self::buscarTransportistasTF(Transportista::getcifTransportista())) /* Modifica */ {
            $consulta = $db->prepare('UPDATE transportistas SET razonSocial = :nombre, idProvincia =:provincia WHERE cif_transportista =:id');
            $consulta->bindParam(':nombre', Transportista::getrazonSocial());
            $consulta->bindParam(':id', Transportista::getcifTransportista());
            $consulta->bindParam(':provincia', Transportista::getprovincia());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al modificar un transportista</p></script>";
                exit();
            }
        } else /* Inserta */ {
            $consulta = $db->prepare('INSERT INTO transportistas (cif_transportista, razonSocial, idProvincia) VALUES(:id, :nombre, :provincia)');
            $consulta->bindParam(':nombre', Transportista::getrazonSocial());
            $consulta->bindParam(':id', Transportista::getcifTransportista());
            $consulta->bindParam(':provincia', Transportista::getprovincia());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al insertar un transportista</p></script>";
                exit();
            }
            Transportista::setcifTransportista($db->lastInsertId());
        }
        self::desconectar($db);
    }

    public static function borrarTransportista() {
        $db = self::conectar();
        $consulta = $db->prepare('DELETE FROM transportistas WHERE cif_transportista = :id');
        $consulta->bindParam(':id', Transportista::getcifTransportista());
        $consulta->execute();
        self::desconectar($db);
    }

    // * * * * * * *  Z O N A   D E   T R A B A J O  * * * * * * * * * 

    public static function leerPertenece($ri, $rf, $ca) {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare('SELECT ' . $ca . ',(SELECT descripcion FROM provincias WHERE provincias.IdProvincia=pertenece.idProvincia) as descripcion,'
                . '(SELECT cif_transportista FROM transportistas WHERE transportistas.cif_transportista=pertenece.cif_transportista) as cif_transportista,'
                . '(SELECT razonSocial FROM transportistas WHERE transportistas.cif_transportista=pertenece.cif_transportista) as razonSocial FROM pertenece limit ' . $ri . ',' . $rf);
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function contarPertenece() {
        // obtengo todas las filas
        $db = self::conectar();
        $re = $db->prepare("SELECT COUNT(*) as total FROM pertenece");
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function buscarZonas($e) {
        $db = self::conectar();
        $re = $db->prepare('SELECT * FROM pertenece WHERE idProvincia = :id');
        $re->bindParam(':id', $e);
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($rows) {
            return new Zona($rows[0]["cif_transportista"], $rows[0]["idProvincia"], $rows[0]["costeKm"]);
        } else {
            return false;
        }
    }

    public static function buscarZonasTF($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT idProvincia FROM pertenece WHERE idProvincia = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch();
        self::desconectar($db);
        if ($registro) {
            return true;
        } else {
            return false;
        }
    }

    public static function buscarZonasTR($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT cif_transportista, razonSocial FROM transportistas WHERE cif_transportista = (SELECT cif_transportista FROM pertenece WHERE idProvincia = :id)');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $registro;
    }

    public static function guardarZonas() {
        $db = self::conectar();
        if (self::buscarZonasTF(Zona::getprovincia())) /* Modifica */ {
            $consulta = $db->prepare('UPDATE pertenece SET cif_transportista = :nombre, costeKm =:coste WHERE idProvincia =:id');
            $consulta->bindParam(':nombre', Zona::getcifTransportista());
            $consulta->bindParam(':id', Zona::getprovincia());
            $consulta->bindParam(':coste', Zona::getcosteKm());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al modificar un transportista</p></script>";
                exit();
            }
        } else /* Inserta */ {
            $consulta = $db->prepare('INSERT INTO pertenece (idProvincia, cif_transportista, costeKm) VALUES(:id, :nombre, :coste)');
            $consulta->bindParam(':nombre', Zona::getcifTransportista());
            $consulta->bindParam(':id', Zona::getprovincia());
            $consulta->bindParam(':coste', Zona::getcosteKm());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al insertar un transportista</p></script>";
                exit();
            }
            Zona::setprovincia($db->lastInsertId());
        }
        self::desconectar($db);
    }

    public static function borrarZona() {
        $db = self::conectar();
        $consulta = $db->prepare('DELETE FROM pertenece WHERE idProvincia = :id');
        $consulta->bindParam(':id', Zona::getprovincia());
        $consulta->execute();
        self::desconectar($db);
    }

    // * * * * * * *  E X P E D I C I O N E S  * * * * * * * * * 
    public static function leerEnvios($ri, $rf, $ca) {
        // obtengo todas las filas
        $db = self::conectar();
        if (isset($_SESSION)) {
            switch ($_SESSION['idRol']) {
                case 1:
                    $re = $db->prepare('SELECT ' . $ca . ' FROM expediciones limit ' . $ri . ',' . $rf);
                    break;
                case 2: //cliente
                    $re = $db->prepare('SELECT ' . $ca . ' FROM expediciones WHERE idCliente = :id limit ' . $ri . ',' . $rf);
                    $re->bindParam(':id', $_SESSION['cliente']);
                    break;
                case 3: //transportista
                    $re = $db->prepare('SELECT ' . $ca . ' FROM expediciones WHERE idTransportista = :id limit ' . $ri . ',' . $rf);
                    $re->bindParam(':id', $_SESSION['transportista']);
                    break;
                case 4:
                    if (!empty($_SESSION['cliente'])) {
                        $re = $db->prepare('SELECT ' . $ca . ' FROM expediciones WHERE idCliente = :id limit ' . $ri . ',' . $rf);
                        $re->bindParam(':id', $_SESSION['cliente']);
                    }
                    if (!empty($_SESSION['transportista'])) {
                        $re = $db->prepare('SELECT ' . $ca . ' FROM expediciones WHERE idTransportista = :id limit ' . $ri . ',' . $rf);
                        $re->bindParam(':id', $_SESSION['transportista']);
                    }
                    break;
            }
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function contarEnvios() {
        // obtengo todas las filas
        $db = self::conectar();
        if (isset($_SESSION)) {
            switch ($_SESSION['idRol']) {
                case 1:
                    $re = $db->prepare("SELECT COUNT(*) as total FROM expediciones");
                    break;
                case 2: //cliente
                    $re = $db->prepare("SELECT COUNT(*) as total FROM expediciones where IdCliente=:id");
                    $re->bindParam(':id', $_SESSION['cliente']);
                    break;
                case 3: //transportista
                    $re = $db->prepare("SELECT COUNT(*) as total FROM expediciones where idTransportista=:id");
                    $re->bindParam(':id', $_SESSION['transportista']);
                    break;
                case 4:
                    if (!empty($_SESSION['cliente'])) {
                    $re = $db->prepare("SELECT COUNT(*) as total FROM expediciones where IdCliente=:id");
                    $re->bindParam(':id', $_SESSION['cliente']);
                    }
                    if (!empty($_SESSION['transportista'])) {
                    $re = $db->prepare("SELECT COUNT(*) as total FROM expediciones where idTransportista=:id");
                    $re->bindParam(':id', $_SESSION['transportista']);
                    }
                    break;
            }
        }
        $re->execute();
        $rows = $re->fetchAll(PDO::FETCH_ASSOC);
        self::desconectar($db);
        return $rows;
    }

    public static function borrarEnvios() {
        $db = self::conectar();
        $consulta = $db->prepare('DELETE FROM expediciones WHERE idExpedicion = :id');
        $consulta->bindParam(':id', Expedicion::getidExpedicion());
        $consulta->execute();
        self::desconectar($db);
    }

    public static function buscarEnvios($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT * FROM expediciones WHERE idExpedicion = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $r = $consulta->fetch(PDO::FETCH_ASSOC);
        self::desconectar($db);
        if ($r) {
            return new Expedicion($r['idExpedicion'], $r['idTransportista'], $r['fecha'], $r['IdCliente'], $r['destinatario'], $r['idProvinciaOrigen'], $r['idProvinciaDestino'], $r['bultos'], $r['peso'], $r['estado'], $r['descripcionIncidencia'], $r['solucionIncidencia'], base64_decode($r['firma']));
        } else {
            return false;
        }
    }

    public static function buscarEnviosTF($e) {
        $db = self::conectar();
        $consulta = $db->prepare('SELECT * FROM expediciones WHERE idExpedicion = :id');
        $consulta->bindParam(':id', $e);
        $consulta->execute();
        $registro = $consulta->fetch();
        self::desconectar($db);
        if ($registro) {
            return true;
        } else {
            return false;
        }
    }

    public static function guardarEnvios() {
        $db = self::conectar();
        if (self::buscarEnviosTF(Expedicion::getidExpedicion())) /* Modifica */ {
            $consulta = $db->prepare('UPDATE expediciones SET '
                    . 'fecha=:fecha, '
                    . 'destinatario=:destinatario, '
                    . 'bultos=:bultos, '
                    . 'peso=:peso, '
                    . 'estado=:estado, '
                    . 'descripcionIncidencia=:descripcionIncidencia, '
                    . 'solucionIncidencia=:solucionIncidencia '
                    . 'WHERE idExpedicion = :idExpedicion');
            $consulta->bindParam(':idExpedicion', Expedicion::getidExpedicion());
            $consulta->bindParam(':fecha', Expedicion::getfecha());
            $consulta->bindParam(':destinatario', Expedicion::getdestinatario());
            $consulta->bindParam(':bultos', Expedicion::getbultos());
            $consulta->bindParam(':peso', Expedicion::getpeso());
            $consulta->bindParam(':estado', Expedicion::getestado());
            $consulta->bindParam(':descripcionIncidencia', Expedicion::getdescripcionIncidencia());
            $consulta->bindParam(':solucionIncidencia', Expedicion::getsolucionIncidencia());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al modificar una expedicion</p></script>";
                exit();
                print $e->getMessage();
            }
        } else /* Inserta */ {
            $consulta = $db->prepare('INSERT INTO expediciones (idExpedicion,idTransportista,fecha,idCliente,destinatario,idProvinciaOrigen,idProvinciaDestino,bultos,peso,estado,descripcionIncidencia,solucionIncidencia) VALUES(:idExpedicion,:idTransportista,:fecha,:idCliente,:destinatario,:idProvinciaOrigen,:idProvinciaDestino,:bultos,:peso,:estado,:descripcionIncidencia,:solucionIncidencia)');
            $consulta->bindParam(':idExpedicion', Expedicion::getidExpedicion());
            $consulta->bindParam(':idTransportista', Expedicion::getidTransportista());
            $consulta->bindParam(':fecha', Expedicion::getfecha());
            $consulta->bindParam(':idCliente', Expedicion::getidCliente());
            $consulta->bindParam(':destinatario', Expedicion::getdestinatario());
            $consulta->bindParam(':idProvinciaOrigen', Expedicion::getidProvinciaOrigen());
            $consulta->bindParam(':idProvinciaDestino', Expedicion::getidProvinciaDestino());
            $consulta->bindParam(':bultos', Expedicion::getbultos());
            $consulta->bindParam(':peso', Expedicion::getpeso());
            $consulta->bindParam(':estado', Expedicion::getestado());
            $consulta->bindParam(':descripcionIncidencia', Expedicion::getdescripcionIncidencia());
            $consulta->bindParam(':solucionIncidencia', Expedicion::getsolucionIncidencia());
            try {
                $consulta->execute();
            } catch (PDOException $e) {
                print "<script><p>error al dar de alta una expedicion</p></script>";
                exit();
            }
            Expedicion::setidExpedicion($db->lastInsertId());
        }
        self::desconectar($db);
    }

    public static function guardarEnviosCF() {
        $db = self::conectar();
        $consulta = $db->prepare('UPDATE expediciones SET '
                . 'firma=:firma '
                . 'WHERE idExpedicion = :idExpedicion');
        $consulta->bindParam(':idExpedicion', Expedicion::getidExpedicion());
        $consulta->bindParam(':firma', base64_encode(Expedicion::getfirma()));
        try {
            $consulta->execute();
        } catch (PDOException $e) {
            print "<script><p>error al modificar una expedicion</p></script>";
            exit();
            print $e->getMessage();
        }
        self::desconectar($db);
    }

}
