<?php
class Provincia {
	private static $_idProvincia;
	private static $_descripcion;


        public function __construct($idProvincia=null, $descripcion=null) {
            self::$_idProvincia = $idProvincia;
            self::$_descripcion = $descripcion;
        }

	public static function getidProvincia() {
		return self::$_idProvincia;
	}
	public static function getdescripcion() {
		return self::$_descripcion;
	}
	
	public static function setidProvincia($idProvincia){
		self::$_idProvincia = $idProvincia;
	}
	public static function setdescripcion($descripcion){
		self::$_descripcion = $descripcion;
	}
}

