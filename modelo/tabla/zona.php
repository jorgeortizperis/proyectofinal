<?php

class Zona {

    private static $_cif_transportista;
    private static $_idProvincia;
    private static $_costeKm;

    public function __construct($cif_transportista = null, $idProvincia = null, $costeKm = null) {
        self::$_cif_transportista = $cif_transportista;
        self::$_idProvincia = $idProvincia;
        self::$_costeKm = $costeKm;
    }

    public static function getcifTransportista() {
        return self::$_cif_transportista;
    }

    public static function getprovincia() {
        return self::$_idProvincia;
    }

    public static function getcosteKm() {
        return self::$_costeKm;
    }

    public static function setcifTransportista($cif_transportista) {
        self::$_cif_transportista = $cif_transportista;
    }

    public static function setprovincia($idProvincia) {
        self::$_idProvincia = $idProvincia;
    }

    public static function setcosteKm($costeKm) {
        self::$_costeKm = $costeKm;
    }

}
