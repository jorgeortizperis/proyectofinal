<?php

class Usuario {

    private static $_dni;
    private static $_nombre;
    private static $_email;
    private static $_password;
    private static $_nivelSeguridad;
    private static $_cif_cliente;
    private static $_cif_transportista;

    public function __construct($dni = null, $nombre = null, $email = null, $password = null, $nivelSeguridad = null, $cif_transportista = null, $cif_cliente = null) {
        self::$_dni = $dni;
        self::$_nombre = $nombre;
        self::$_email = $email;
        self::$_password = $password;
        self::$_nivelSeguridad = $nivelSeguridad;
        self::$_cif_cliente = $cif_cliente;
        self::$_cif_transportista = $cif_transportista;
    }

    public static function getdni() {
        return self::$_dni;
    }

    public static function getnombre() {
        return self::$_nombre;
    }

    public static function getemail() {
        return self::$_email;
    }

    public static function getpassword() {
        return self::$_password;
    }

    public static function getnivelSeguridad() {
        return self::$_nivelSeguridad;
    }

    public static function getncifCliente() {
        return self::$_cif_cliente;
    }   
    public static function getcifTransportista() {
        return self::$_cif_transportista;
    }
    
    public static function setdni($dni) {
        self::$_dni = $dni;
    }

    public static function setnombre($nombre) {
        self::$_nombre = $nombre;
    }

    public static function setemail($email) {
        self::$_email = $email;
    }

    public static function setpassword($password) {
        self::$_password = $password;
    }

    public static function setnivelSeguridad($nivelSeguridad) {
        self::$_nivelSeguridad = $nivelSeguridad;
    }

    public static function setcifCliente($cifCliente) {
        self::$_cif_cliente = $cifCliente;
    }
    
    public static function setcifTransportista($cifTransportista) {
        self::$_cif_transportista = $cifTransportista;
    }
}
