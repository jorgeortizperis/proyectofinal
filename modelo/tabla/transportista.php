<?php
class Transportista {
	private static $_cif_transportista;
	private static $_razonSocial;
	private static $_idProvincia;

        public function __construct($cif_transportista=null, $razonSocial=null, $idProvincia=null) {
            self::$_cif_transportista = $cif_transportista;
            self::$_razonSocial = $razonSocial;
            self::$_idProvincia = $idProvincia; 
        }

	public static function getcifTransportista() {
		return self::$_cif_transportista;
	}
	public static function getrazonSocial() {
		return self::$_razonSocial;
	}
	public static function getprovincia() {
		return self::$_idProvincia;
	}
	
	public static function setcifTransportista($cif_transportista){
		self::$_cif_transportista = $cif_transportista;
	}
	public static function setrazonSocial($razonSocial){
		self::$_razonSocial = $razonSocial;
	}
	public static function setprovincia($idProvincia){
		self::$_idProvincia = $idProvincia;
	}
}