<?php

class Expedicion {

    private static $_idExpedicion;
    private static $_idTransportista;
    private static $_fecha;
    private static $_idCliente;
    private static $_destinatario;
    private static $_idProvinciaOrigen;
    private static $_idProvinciaDestino;
    private static $_bultos;
    private static $_peso;
    private static $_estado;
    private static $_descripcionIncidencia;
    private static $_solucionIncidencia;
    private static $_firma;

    public function __construct($idExpedicion = null, $idTransportista = null, $fecha = null, $idCliente = null, $destinatario = null, $idProvinciaOrigen = null, $idProvinciaDestino = null, $bultos = null, $peso = null, $estado = null, $descripcionIncidencia = null, $solucionIncidencia = null, $firma = null) {
        self::$_idExpedicion = $idExpedicion;
        self::$_idTransportista = $idTransportista;
        self::$_fecha = $fecha;
        self::$_idCliente = $idCliente;
        self::$_destinatario = $destinatario;
        self::$_idProvinciaOrigen = $idProvinciaOrigen;
        self::$_idProvinciaDestino = $idProvinciaDestino;
        self::$_bultos = $bultos;
        self::$_peso = $peso;
        self::$_estado = $estado;
        self::$_descripcionIncidencia = $descripcionIncidencia;
        self::$_solucionIncidencia = $solucionIncidencia;
        self::$_firma = $firma;
    }

    public static function getidExpedicion() {
        return self::$_idExpedicion;
    }

    public static function getidTransportista() {
        return self::$_idTransportista;
    }

    public static function getfecha() {
        return self::$_fecha;
    }

    public static function getidCliente() {
        return self::$_idCliente;
    }

    public static function getdestinatario() {
        return self::$_destinatario;
    }

    public static function getidProvinciaOrigen() {
        return self::$_idProvinciaOrigen;
    }

    public static function getidProvinciaDestino() {
        return self::$_idProvinciaDestino;
    }

    public static function getbultos() {
        return self::$_bultos;
    }

    public static function getpeso() {
        return self::$_peso;
    }

    public static function getestado() {
        return self::$_estado;
    }

    public static function getdescripcionIncidencia() {
        return self::$_descripcionIncidencia;
    }

    public static function getsolucionIncidencia() {
        return self::$_solucionIncidencia;
    }

    public static function getfirma() {
        return self::$_firma;
    }

    public static function setidExpedicion($idExpedicion) {
        self::$_idExpedicion = $idExpedicion;
    }

    public static function setidTransportista($idTransportista) {
        self::$_idTransportista = $idTransportista;
    }

    public static function setfecha($fecha) {
        self::$_fecha = $fecha;
    }

    public static function setidCliente($idCliente) {
        self::$_idCliente = $idCliente;
    }

    public static function setdestinatario($destinatario) {
        self::$_destinatario = $destinatario;
    }

    public static function setidProvinciaOrigen($idProvinciaOrigen) {
        self::$_idProvinciaOrigen = $idProvinciaOrigen;
    }

    public static function setidProvinciaDestino($idProvinciaDestino) {
        self::$_idProvinciaDestino = $idProvinciaDestino;
    }

    public static function setbultos($bultos) {
        self::$_bultos = $bultos;
    }

    public static function setpeso($peso) {
        self::$_peso = $peso;
    }

    public static function setestado($estado) {
        self::$_estado = $estado;
    }

    public static function setdescripcionIncidencia($descripcionIncidencia) {
        self::$_descripcionIncidencia = $descripcionIncidencia;
    }

    public static function setsolucionIncidencia($solucionIncidencia) {
        self::$_solucionIncidencia = $solucionIncidencia;
    }

    public static function setfirma($firma) {
        self::$_firma = $firma;
    }

}
