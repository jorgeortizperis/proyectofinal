<?php
class Cliente {
    private static $_cifCliente;
    private static $_razonSocial;

    public function __construct($cifCliente = null, $razonSocial = null) {
        self::$_cifCliente = $cifCliente;
        self::$_razonSocial = $razonSocial;
    }

    public static function getcifCliente() {
        return self::$_cifCliente;
    }

    public static function getrazonSocial() {
        return self::$_razonSocial;
    }

    public static function setcifCliente($cifCliente) {
        self::$_cifCliente = $cifCliente;
    }

    public static function setrazonSocial($razonSocial) {
        self::$_razonSocial = $razonSocial;
    }

}
