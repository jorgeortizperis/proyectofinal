<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/modelo/interface/sql.php';
$json = false;
if (isset($_POST)) {
    $n = "leer" . $_POST['me'];
    $salida = mySql::$n($_POST['ri'], $_POST['rf'], $_POST['ca']);
    $n = "contar" . $_POST['me'];
    $contador = mySql::$n();
    $d = array('cuantos' => $contador, 'registros' => $salida);
    $json = json_encode($d);
} else {
    $json = false;
}
echo $json;
?>