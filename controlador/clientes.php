<?php

class Cliente_c {

    public function cliente() {
        $contenidoParrilla = self::parrilla(); // pasarle a la parrilla de donde tiene que sacar los datos, $contenidoParrilla
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        require_once Config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    function parrilla() {
        return $c = "var parrilla = new Parrilla();
            parrilla.Start('Mantenimiento de Clientes.','Clientes', limites = ['5', '5', '15', '20'], //limites=> el primero es el valor por defecto seleccionado
            [['cif_cliente', 'CIF', true,true],
            ['razonSocial', 'Razón Social', false,true]]);";
    }

    public function alta() {
        self::fi();
        $cliente_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($cliente_id) {
            $cli = mySql::buscarClientes($cliente_id);
        } else {
            $cli = new Cliente();
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $cliId = (isset($_POST['cif_cliente'])) ? $_POST['cif_cliente'] : null;
            $nombre = (isset($_POST['razonSocial'])) ? $_POST['razonSocial'] : null;
            $cli->setcifCliente($cliId);
            $cli->setrazonSocial($nombre);
            mySql::guardarClientes();
            header('Location: controlador.php?ctl=cliente');
        } else {
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_clientes.php';
        }
    }

    public function borrar() {
        self::fi();
        $cliente_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($cliente_id) {
            $cliente = mysql::buscarClientes($cliente_id);
            mySql::borrarCliente();
            header('Location: controlador.php?ctl=cliente');
        }
    }
        public function buscar() {
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/cliente.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $usu = new Cliente();
            $usu= mySql::buscarClientesTF($_POST['f']);
            if ($usu) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/cliente.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
