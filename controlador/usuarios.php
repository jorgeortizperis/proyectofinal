<?php

class Usuario_c {

    public function usuario() {
        if (!isset($_SESSION)) {
            session_start();
        }
        $contenidoParrilla = self::parrilla(); // pasarle a la parrilla de donde tiene que sacar los datos, $contenidoParrilla
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        //require_once Config::sitio() . '/vista/page.php';
        require_once Config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    public function parrilla() {
        return $c = "var parrilla = new Parrilla();
            parrilla.Start('Mantenimiento de Usuarios.','Usuarios', limites = ['5', '5', '15', '20'], //limites=> el primero es el valor por defecto seleccionado
            [['dni', 'DNI', true,true],
            ['nombre', 'Nombre', false,true],
            ['email', 'E-mail', false,true],
            ['idRol', 'Seguridad', false,true]]);";
    }

    public function alta() {
        self::fi();
        $usuario_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null;
        if ($usuario_id) {
            $usu = mySql::buscarUsuarios($usuario_id);
        } else {
            $usu = new Usuario();
            if (!empty($_SESSION['cliente']) || (!empty($_SESSION['transportista'])) && ($_SESSION['idRol'] > 1)) {
                !empty($_SESSION['cliente']) ? $usu->setcifCliente($_SESSION['cliente']) : null;
                !empty($_SESSION['transportista']) ? $usu->setcifTransportista($_SESSION['transportista']) : null;
                $usu->setnivelSeguridad("4");
            }
        }
        $rol = mySql::buscarRol();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $dni = (isset($_POST['dni'])) ? $_POST['dni'] : null;
            $nombre = (isset($_POST['nombre'])) ? $_POST['nombre'] : null;
            $email = (isset($_POST['email'])) ? $_POST['email'] : null;
            $cli = (isset($_POST['cli'])) ? $_POST['cli'] : null;
            $tra = (isset($_POST['tra'])) ? $_POST['tra'] : null;
            $ni = (isset($_POST['ni'])) ? $_POST['ni'] : null;
            $pass = (isset($_POST['pass'])) ? md5($_POST['pass']) : null;
            $ir = (isset($_POST['idRol'])) ? $_POST['idRol'] : null;
            $irr = $_SESSION['idRol'] == 1 ? $ir : $_POST['ni'];
            $usu->setdni($dni);
            $usu->setnombre($nombre);
            $usu->setemail($email);
            $usu->setpassword($pass);
            $usu->setnivelSeguridad($irr);
            switch ($ir) {
                case 2:
                    $usu->setcifCliente($cli);
                    $usu->setcifTransportista(null);
                    break;
                case 3:
                    $usu->setcifTransportista($tra);
                    $usu->setcifCliente(null);
                    break;
            }
            mySql::guardarUsuarios();
            header('Location: controlador.php?ctl=usuario');
        } else {
            //include_once Config::sitio() . '/vista/v_usuarios.php';
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_usuarios.php';
        }
    }

    public function borrar() {
        self::fi();
        $usuario_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($usuario_id) {
            $usuario = mysql::buscarUsuarios($usuario_id);
            mySql::borrarUsuarios();
            header('Location: controlador.php?ctl=usuario');
        }
    }

    public function buscar() {
        //require_once Config::sitio() . '/modelo/tabla/usuario.php';
        //require_once Config::sitio() . '/modelo/interface/sql.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/usuario.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $usu = new Usuario();
            $usu = mySql::buscarUsuariosTF($_POST['f']);
            if ($usu) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    public function buscarTipo() {
        //require_once Config::sitio() . '/modelo/tabla/cliente.php';
        //require_once Config::sitio() . '/modelo/tabla/transportista.php';
        //require_once Config::sitio() . '/modelo/interface/sql.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/cliente.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/transportista.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            switch ($_POST['f']) {
                case "2":
                    $r = new Cliente();
                    $r = mySql::buscarClientes(null);
                    $p = $p . '<option value="" disabled selected>Elige tipo de usuario</option>';
                    foreach ($r as $k) {
                        $p = $p . "<option value='" . $k['cif_cliente'] . "'>" . $k['razonSocial'] . "</option>";
                    }
                    break;
                case "3":
                    $r = new Transportista();
                    $r = mySql::buscarTransportistas(null);
                    $p = $p . '<option value="" disabled selected>Elige tipo de usuario</option>';
                    foreach ($r as $k) {
                        $p = $p . "<option value='" . $k['cif_transportista'] . "'>" . $k['razonSocial'] . "</option>";
                    }
                    break;
            }
            echo $p;
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        //require_once Config::sitio() . '/modelo/tabla/usuario.php';
        //require_once Config::sitio() . '/modelo/interface/sql.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/usuario.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
