<?php

class Controller {

    public function login() {
        if (!isset($_SESSION)) {
            session_start();
        }
        require_once 'validarse.php';
        if (!c_validarse::validar()) {
            echo'<script>alert("datos erroneos");</script>';
            header('location:../login.html');
        } else {
            if (isset($_COOKIE['exp'])) {
                header('Location: controlador.php?ctl=movil&sr=' . $_COOKIE['exp']);
            } else {
                header('Location: controlador.php?ctl=inicio');
            }
        }
    }

    public function inicio() {
        if (!isset($_SESSION)) {
            session_start();
        }
        //$_SESSION['idRol'] = $row['idRol'];
        switch ($_SESSION['idRol']) {// Segun el tipo de Rol tendran un tipo de menu
            case 1: //root
                $menu = array(
                    array('m' => 'controlador.php?ctl=usuario', 'op' => 'Usuarios'),
                    array('m' => 'controlador.php?ctl=cliente', 'op' => 'Clientes'),
                    array('m' => 'controlador.php?ctl=transportista', 'op' => 'Transportistas'),
                    array('m' => 'controlador.php?ctl=zona', 'op' => 'Zona trabajo'),
                    array('m' => 'controlador.php?ctl=expedicion', 'op' => 'Expediciones'));
                break;
            case 2: //cliente
                $menu = array(
                    array('m' => 'controlador.php?ctl=usuario', 'op' => 'Usuarios'),
                    array('m' => 'controlador.php?ctl=expedicion', 'op' => 'Expediciones'));
                break;
            case 3: //transportista
                $menu = array(
                    array('m' => 'controlador.php?ctl=usuario', 'op' => 'Usuarios'),
                    array('m' => 'controlador.php?ctl=expedicion', 'op' => 'Expediciones'));
                break;
            case 4: //operario
                $menu = array(
                    array('m' => 'controlador.php?ctl=expedicion', 'op' => 'Expediciones'));
                break;
        }
        $contenidoParrilla = "";
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        $_SESSION['me'] = $menu; // guardamos el menu en $_session para poder tenerlo para todos, la otra opcion seria hacer una clase.
        require config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    public function cerrar() {
        require Config::sitio() . '/proyectoFinalModulo/controlador/salir.php';
    }

    public function contacto() {
        //print ("<html><body><script>alert('" . print_r($_POST) . "');</script></body></html>");
        $mensaje = "Se ha recibido correctamente tu CV.";
        $email_a = 'jorgeortizperis@hotmail.com';
        $email_asunto = "Mensaje de contacto desde la web, de: " . $_POST['nombre'];
        $email_message = $_POST['mensaje'];
        $cabeceras = 'From: ' . $_POST['mail'] . "\r\n" .
                'Reply-To: jorgeortizperis@hotmail.com' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
        if (mail($email_a, $email_asunto, $email_message, $cabeceras)) {
            print ("<html><body><script>alert('Se ha recibido correctamente tu email.');</script></body></html>");
        }
        header('location: ../index.html');
        /*
         * Vamos a ver como configurar XAMPP en Windows para enviar correos en nuestros desarrollos de sitios Drupal.
          Aclarar que lo he probado en Windows no sé si funciona en Mac o Linux y que no lo he hecho para el envío de correos masivos.

          Para comenzar tenemos que editar dos archivos, php.ini y sendmail.ini.
          Configuración del archivo php.ini

          Edito archivo que se encuentra en: xampp/php/

          Tengo que modificar las siguientes líneas:
          ;sendmail_path = ""C:\xampp\sendmail\sendmail.exe" -t"

          Borro el punto y coma del comienzo.
          sendmail_path = ""C:\xampp\sendmail\sendmail.exe" -t"

          Luego busco la línea siguiente:
          sendmail_path="C:\xampp\mailtodisk\mailtodisk.exe"

          Y la comentó al principio con un ;
          ;sendmail_path="C:\xampp\mailtodisk\mailtodisk.exe"

          Guardo y cierro este archivo.
          Configuración del archivo sendmail.ini

          Ahora edito este archivo que se encuentra en: xampp/sendmail/

          Tengo que modificar las siguientes líneas:
          smtp_server=mail.mydomain.com

          Como yo utilizo Gmail voy a poner lo siguiente:
          smtp_server=smtp.gmail.com

          Ahora tenemos que especificar el puerto de Gmail:
          smtp_port=25

          Por
          smtp_port=587

          Para finalizar tenemos que modificar las siguientes dos líneas:
          auth_username=
          auth_password=

          Aquí tenemos que poner nuestra dirección de correo y a continuación nuestra contraseña de Gmail.

          Guardar el archivo.
          Reiniciar el servidor

          Para terminar tenemos que reiniciar el servidor para que los cambios hagan efecto.

         */
    }

}
