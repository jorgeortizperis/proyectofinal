<?php

if (!isset($_SESSION)) {
    session_start();
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/proyectoFinalModulo/modelo/interface/sql.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/proyectoFinalModulo/modelo/tabla/zona.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/proyectoFinalModulo/modelo/tabla/transportista.php';
$json = false;
if (isset($_POST)) {
    switch ($_POST['q']) {
        case "Actualizar":
            $salida = mySql::buscarProvincia($_POST['e']); //hay que ponerle un valor para que coja correctamente la SQL
            break;
        case "Comprobar":
            $s = mySql::buscarZonasTF($_POST['e']);
            $salida = $s ? "false" : "true";
            break;
        case "Transportista":
            $salida = mySql::buscarZonasTR($_POST['e']);
            break;
    }
    $d = array('registros' => $salida);
    $json = json_encode($d);
} else {
    $json = false;
}
echo $json;
?>