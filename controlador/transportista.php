<?php

class Transportista_c {

    public function transportista() {
        $contenidoParrilla = self::parrilla(); // pasarle a la parrilla de donde tiene que sacar los datos, $contenidoParrilla
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        require_once Config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    function parrilla() {
        return $c = "var parrilla = new Parrilla();
            parrilla.Start('Mantenimiento de Transportistas.','Transportistas', limites = ['5', '5', '15', '20'], //limites=> el primero es el valor por defecto seleccionado
            [['cif_transportista', 'CIF', true, true],
            ['razonSocial', 'Razón Social', false,true],
            ['idProvincia', 'Cod. Provincia', false,true],
            ['descripcion', 'Provincia', false,false]]);";
    }

    public function alta() {
        self::fi();
        $transportista_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($transportista_id) {
            $tra = mySql::buscarTransportistas($transportista_id);
        } else {
            $tra = new Transportista();
        }
        $pro = mySql::buscarProvincia(null);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $traId = (isset($_POST['cif_transportista'])) ? $_POST['cif_transportista'] : null;
            $nombre = (isset($_POST['razonSocial'])) ? $_POST['razonSocial'] : null;
            $proId = (isset($_POST['provincia'])) ? $_POST['provincia'] : null;
            $tra->setcifTransportista($traId);
            $tra->setrazonSocial($nombre);
            $tra->setprovincia($proId);
            mySql::guardarTransportistas();
            header('Location: controlador.php?ctl=transportista');
        } else {
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_transportistas.php';
        }
    }

    public function borrar() {
        self::fi();
        $transportista_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($transportista_id) {
            $transportista = mysql::buscarTransportistas($transportista_id);
            mySql::borrarTransportista();
            header('Location: controlador.php?ctl=transportista');
        }
    }

    public function buscar() {
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/transportista.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $usu = new Transportista();
            $usu = mySql::buscarTransportistasTF($_POST['f']);
            if ($usu) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/transportista.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
