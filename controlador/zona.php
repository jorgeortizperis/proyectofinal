<?php

class Zona_c {

    public function zona() {
        $contenidoParrilla = self::parrilla(); // pasarle a la parrilla de donde tiene que sacar los datos, $contenidoParrilla
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        require_once Config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    public function parrilla() {
        return $c = "var parrilla = new Parrilla();
            parrilla.Start('Mantenimiento de Zonas.','Pertenece', limites = ['5', '5', '15', '20'], //limites=> el primero es el valor por defecto seleccionado
            [['idProvincia', 'Cod. Provincia', true,true],
             ['descripcion', 'Provincia', false,false],
             ['cif_transportista', 'CIF Transportista', false,false],
             ['razonSocial', 'Razón social', false,false]]);";
    }

    public function alta() {
        self::fi();
        $transportista_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($transportista_id) {
            $zo = mySql::buscarZonas($transportista_id); //hay que ponerle un valor para que coja correctamente la sql
        } else {
            $zo = new Zona();
        }
        $pro = mySql::buscarProvincia("no nulo"); // hay que pasarle un valor para que nos devuelva solo las provincias no asignadas a otros transportistas
        $tra = mySql::buscarTransportistas(null);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $traId = (isset($_POST['cif_transportista'])) ? $_POST['cif_transportista'] : null;
            $proId = (isset($_POST['provincia'])) ? $_POST['provincia'] : null;
            $coste = (isset($_POST['costeKm'])) ? $_POST['costeKm'] : null;
            $zo->setcifTransportista($traId);
            $zo->setprovincia($proId);
            $zo->setcosteKm($coste);
            mySql::guardarZonas();
            header('Location: controlador.php?ctl=zona');
        } else {
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_zonas.php';
        }
    }

    public function borrar() {
        self::fi();
        $zona_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($zona_id) {
            $zona = mysql::buscarZonas($zona_id);
            mySql::borrarZona();
            header('Location: controlador.php?ctl=zona');
        }
    }

    public function provin() {
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/provincia.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $pro = new Provincia();
            $pro = mySql::buscarProvincia(null);
            $p = "";
            foreach ($pro as $k) {
                $p = $p . "<option value='" . $k['IdProvincia'] . "'>" . $k['descripcion'] . "</option>";
            }
            echo $p;
        }
    }

    public function google() {
        $origen = 'Torrente';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $destino = $_POST['destino'];
            $url = 'http://maps.googleapis.com/maps/api/directions/json?origin=' . urlencode($origen) . '&destination=' . urlencode($destino) . '&region=es&sensor=false&mode=driving';
            $datos = @file_get_contents($url);
            $jsondata = json_decode($datos, true);
            $salida = array();
            if (isset($jsondata) and is_array($jsondata)) {
                echo "<p>Distancia hasta destino: <a id='IdValor'>" . round($jsondata['routes'][0]['legs'][0]['distance']['value'] / 1000) . "</a> Kms </p>";
            }
        }
    }

    public function valorar() {
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/zona.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/provincia.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $zona = new Zona();
            $pro = new Provincia();
            $zona = mySql::buscarZonas($_POST['c']);
            $pro = mySql::buscarProvinciaN($_POST['c']);
            if ($zona) {
                echo 'El envío a ' . $pro->getdescripcion() . ' tendría un valor de: <a>' . round($zona->getcosteKm() * $_POST['f'] * $_POST['p'], 2, PHP_ROUND_HALF_UP) . '</a> Euros';
            } else {
                echo 'Lo sentimos, no trabajamos en esa provincia.';
            }
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/zona.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/transportista.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
