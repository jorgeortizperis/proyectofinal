<?php

if (!isset($_SESSION)) {
    session_start();
}
// carga del modelo y los controladores
require_once $_SERVER['DOCUMENT_ROOT'] . '/proyectoFinalModulo/config/config.php';
require_once Config::sitio() . '/proyectoFinalModulo/controlador/accion.php';
require_once Config::sitio() . '/proyectoFinalModulo/controlador/usuarios.php';
require_once config::sitio() . '/proyectoFinalModulo/controlador/clientes.php';
require_once config::sitio() . '/proyectoFinalModulo/controlador/transportista.php';
require_once config::sitio() . '/proyectoFinalModulo/controlador/expediciones.php';
require_once config::sitio() . '/proyectoFinalModulo/controlador/zona.php';
require_once config::sitio() . '/proyectoFinalModulo/controlador/qr.php';

// enrutamiento
$map = array(
    'login' => array('controller' => 'Controller', 'action' => 'login'),
    'contacto' => array('controller' => 'Controller', 'action' => 'contacto'),     
    'cerrar' => array('controller' => 'Controller', 'action' => 'cerrar'),
    'inicio' => array('controller' => 'Controller', 'action' => 'inicio'),
    
    'usuario' => array('controller' => 'Usuario_c', 'action' => 'usuario'),
    'aUsuarios' => array('controller' => 'Usuario_c', 'action' => 'alta'),
    'eUsuarios' => array('controller' => 'Usuario_c', 'action' => 'borrar'),
    'comprobarDniU' => array('controller' => 'Usuario_c', 'action' => 'buscar'),
    'buscarTipoU' => array('controller' => 'Usuario_c', 'action' => 'buscarTipo'),
    
    'cliente' => array('controller' => 'Cliente_c', 'action' => 'cliente'),
    'aClientes' => array('controller' => 'Cliente_c', 'action' => 'alta'),
    'eClientes' => array('controller' => 'Cliente_c', 'action' => 'borrar'),
    'comprobarDniC' => array('controller' => 'Cliente_c', 'action' => 'buscar'),
    
    'transportista' => array('controller' => 'Transportista_c', 'action' => 'transportista'),
    'aTransportistas' => array('controller' => 'Transportista_c', 'action' => 'alta'),
    'eTransportistas' => array('controller' => 'Transportista_c', 'action' => 'borrar'),
    'comprobarDniT' => array('controller' => 'Transportista_c', 'action' => 'buscar'),
    
    'zona' => array('controller' => 'Zona_c', 'action' => 'zona'),
    'aPertenece' => array('controller' => 'Zona_c', 'action' => 'alta'),
    'ePertenece' => array('controller' => 'Zona_c', 'action' => 'borrar'),
    'valorar' => array('controller' => 'Zona_c', 'action' => 'valorar'),
    'provin' => array('controller' => 'Zona_c', 'action' => 'provin'),
    'google' => array('controller' => 'Zona_c', 'action' => 'google'),
    
    'expedicion' => array('controller' => 'Expedicion_c', 'action' => 'expedicion'),
    'aEnvios' => array('controller' => 'Expedicion_c', 'action' => 'alta'),
    'eEnvios' => array('controller' => 'Expedicion_c', 'action' => 'borrar'),
    'seguimiento' => array('controller' => 'Expedicion_c', 'action' => 'seguimiento'),
    
    'movil' => array('controller' => 'Qr_c', 'action' => 'm'),
    'imagen' => array('controller' => 'Qr_c', 'action' => 'imagen')
);

if (isset($_GET['exp'])) {
    setcookie('exp', $_GET['exp'], time() + (120)); // tiene una duracion de 2 minutos
}
// Parseo de la ruta
if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' . $_GET['ctl'] . '</p></body></html>';
        exit;
    }
}

$controlador = $map[$ruta];
// Ejecución del controlador asociado a la ruta

if (method_exists($controlador['controller'], $controlador['action'])) {
    // la siguiente linea seria como: call_user_func(array (new Controller, 'insertar')), es decir instanciamos a la clase Controller la accion.
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {

    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' . $controlador['controller'] . '->' . $controlador['action'] . '</i> no existe</h1></body></html>';
}