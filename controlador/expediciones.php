<?php

class Expedicion_c {

    public function expedicion() {
        $contenidoParrilla = self::parrilla(); // pasarle a la parrilla de donde tiene que sacar los datos, $contenidoParrilla
        $contenidoCuerpo = "";
        $contenidoJavasCript = "";
        require_once Config::sitio() . '/proyectoFinalModulo/vista/page.php';
    }

    public function parrilla() {
        return $c = "var parrilla = new Parrilla();
            parrilla.Start('Mantenimiento de Envios.','Envios', limites = ['5', '5', '15', '20'], //limites=> el primero es el valor por defecto seleccionado
            [['idExpedicion', 'Expedición', true,true],
            ['fecha', 'Fecha', false,true],
            ['destinatario', 'Destinatario', false,true],
            ['estado', 'Estado', false,true]]);";
    }

    public function alta() {
        self::fi();
        $expedicion_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        $pro = mySql::buscarProvincia(null);
        $tra = mySql::buscarTransportistas(null);
        $cli = mySql::buscarClientes(null);
        if ($expedicion_id) {
            $expe = mySql::buscarEnvios($expedicion_id);
            $tra = mySql::buscarTransportistas($expe->getidTransportista());
        } else {
            $expe = new Expedicion();
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $e1 = (isset($_POST['idExpedicion'])) ? $_POST['idExpedicion'] : null;
            $e2 = (isset($_POST['idTransportista'])) ? $_POST['idTransportista'] : null;
            $e3 = (isset($_POST['fecha'])) ? $_POST['fecha'] : null;
            $e4 = (isset($_POST['idCliente'])) ? $_POST['idCliente'] : null;
            $e5 = (isset($_POST['destinatario'])) ? $_POST['destinatario'] : null; //
            $e6 = (isset($_POST['idProvinciaOrigen'])) ? $_POST['idProvinciaOrigen'] : null;
            $e7 = (isset($_POST['idProvinciaDestino'])) ? $_POST['idProvinciaDestino'] : null;
            $e8 = (isset($_POST['bultos'])) ? $_POST['bultos'] : 0;
            $e9 = (isset($_POST['peso'])) ? $_POST['peso'] : 0;
            $e10 = (isset($_POST['estado'])) ? $_POST['estado'] : "en plaza destino";
            $e11 = (isset($_POST['descripcionIncidencia'])) ? $_POST['descripcionIncidencia'] : null;
            $e12 = (isset($_POST['solucionIncidencia'])) ? $_POST['solucionIncidencia'] : null;
            $expe->setidExpedicion($e1);
            $expe->setidTransportista($e2);
            $expe->setfecha($e3);
            $expe->setidCliente($e4);
            $expe->setdestinatario($e5);
            $expe->setidProvinciaOrigen($e6);
            $expe->setidProvinciaDestino($e7);
            $expe->setbultos($e8);
            $expe->setpeso($e9);
            $expe->setestado($e10);
            $expe->setdescripcionIncidencia($e11);
            $expe->setsolucionIncidencia($e12);
            mySql::guardarEnvios();
            header('Location: controlador.php?ctl=expedicion');
        } else {
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_envios.php';
        }
    }

    public function borrar() {
        self::fi();
        $expedicion_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($expedicion_id) {
            $expedicion = mysql::buscarEnvios($expedicion_id);
            mySql::borrarEnvios();
            header('Location: controlador.php?ctl=expedicion');
        }
    }

    public function seguimiento() {
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/expedicion.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $expe = new Expedicion();
            $expe = mySql::buscarEnvios($_POST['f']);
            if ($expe) {
                echo '<p>La expedición <a>' . $expe->getidExpedicion() . '</a> se encuentra en el estado de: <a>' . $expe->getestado() . ' </a></p>';
            } else {
                echo 'Expedición no encontrada.';
            }
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/cliente.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/provincia.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/transportista.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/expedicion.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
