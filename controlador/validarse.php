<?php

require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/usuario.php';
require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/expedicion.php';

class c_validarse {

    function validar() {
        if ((!empty($_POST['dni'])) && (!empty($_POST['password'])) || (isset($_COOKIE['ot']))) {
            $a = new mySql();
            $re = $a->leerU();
            foreach ($re as $row) {//recorremos el array devuelto para localizar el usuario y su contraseña
                if (isset($_COOKIE['ot'])) {
                    if (($row['cif_transportista'] == $_COOKIE['ot'])) {
                        if (isset($_COOKIE['exp'])) {
                            $expe = new Expedicion();
                            $expe = mySql::buscarEnvios($_COOKIE['exp']);
                            setcookie('exp', "", time() - (3600 * 24)); //matamos la cookie de envios por seguridad
                            if ($row['cif_transportista'] == ($expe->getidExpedicion())) {
                                self::co($row);
                                return true;
                            }
                        }
                    }
                }
                if ((!empty($_POST['dni'])) && (!empty($_POST['password']))) {
                    if (($row['dni'] == $_POST['dni']) && ($row['contrasena'] == md5($_POST['password']))) {
                        // && (hash_equals($row['password'], crypt($_POST['password'], $row['password'])))
                        if (isset($_COOKIE['exp'])) {
                            $expe = new Expedicion();
                            $expe = mySql::buscarEnvios($_COOKIE['exp']);
                            setcookie('exp', "", time() - (3600 * 24)); //matamos la cookie de envios por seguridad
                            if ($row['cif_transportista'] == ($expe->getidTransportista())) {
                                self::co($row);
                                return true;
                            }
                        } else {
                            self::co($row);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    function co($row) {
        session_start();
        $_SESSION['id'] = $row['dni'];
        $_SESSION['nombre'] = $row['nombre'];
        $_SESSION['password'] = $row['password'];
        $_SESSION['idRol'] = $row['idRol'];
        $_SESSION['cliente'] = $row['cif_cliente'];
        $_SESSION['transportista'] = $row['cif_transportista'];
        if (($row['idRol'] == "4") && (!empty($row['cif_transportista']))) {
            self::cookieT($row['cif_transportista']);
        }
    }

    function cookieT($e) {
        if (empty($_COOKIE["ot"])) {//creamos la cookie si es la primera vez
            setcookie("ot", $e, time() + (3600 * 24)); // tiene una duracion de un dia
        } else {
            setcookie("ot", $e, time() + (3600 * 24)); // tiene una duracion de un dia
        }
    }

}
