<?php

class Qr_c {

    public function m() {
        self::fi();
        $expe_id = (isset($_REQUEST['sr'])) ? $_REQUEST['sr'] : null; //cuando hacemos doble clik en la parrilla
        if ($expe_id) {
            $expe = mySql::buscarEnvios($expe_id);
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $e10 = (isset($_POST['estado'])) ? $_POST['estado'] : "en plaza destino";
            $e11 = (isset($_POST['descripcionIncidencia'])) ? $_POST['descripcionIncidencia'] : null;
            $expe->setestado($e10);
            $expe->setdescripcionIncidencia($e11);
            mySql::guardarEnvios();
            header('Location: controlador.php?ctl=cerrar');
        } else {
            include_once Config::sitio() . '/proyectoFinalModulo/vista/v_qr.php';
        }
    }

    public function imagen() {
        self::fi();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $expe = mySql::buscarEnvios($_REQUEST['f']);
            if (array_key_exists('img', $_REQUEST)) {
                $expe->setfirma(substr($_REQUEST['img'], 22));
            } else {
                $expe->setfirma(null);
            }
            mySql::guardarEnviosCF();
        }
    }

    function fi() {
        if (!isset($_SESSION)) {
            session_start();
        }
        if (empty($_SESSION['nombre'])) {
            if (!isset($_COOKIE['ot'])) {
                header('location:../login.html');
            } else {
                header('Location: controlador.php?ctl=login');
            }
        }
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/tabla/expedicion.php';
        require_once Config::sitio() . '/proyectoFinalModulo/modelo/interface/sql.php';
    }

}
